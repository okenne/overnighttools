#!/usr/bin/env python3
from .__loggersetup__ import Logger  # Sets up logging
from .__configsetup__ import ConfigLoad  # Loads the config file
# These allow for comparing times to determine reload times
from datetime import datetime
from datetime import date
from dateutil.relativedelta import *

# The following items are for processing the configuration file
import os  # Assists with filesystem items
# The following items are for setting up file and console logging
import sys  # Script input call

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


def TimeCalculate(ReloadTime, Format, Offset=0):
    # Reload time is expected as HH:MM and format is minutes or hours
    # Alternatively, ReloadTime can be provided as "now"- this will statically define a 5 minute timer

    if "now" in ReloadTime:
        TimeMinutes = 5 + Offset

    else:
        # Remove leading zeroes as relativedelta doesn't take these
        ReloadTime = ReloadTime.lstrip("0")

        # Convert the time to minutes from now
        TimeMinutes = int((date.today()+relativedelta(hour=int(ReloadTime.split(":")[0]), minute=int(ReloadTime.split(":")[1])) - datetime.now()).total_seconds() // 60 + Offset)

        # Check if the above is negative as relativedelta doesn't return the next occurrence of a time
        if TimeMinutes < 0:
            TimeMinutes = TimeMinutes + 1440

    # Check the format requested
    if Format is 'minutes':
        return TimeMinutes
    elif Format is 'hours':
        hour, minute = divmod(TimeMinutes, 60)
        return f'{hour}:{minute}'
    elif Format is 'seconds':
        return TimeMinutes*60
    else:
        OutputLog.error(f'Unable to determine format {Format}!')
        return
