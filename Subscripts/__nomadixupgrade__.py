#!/usr/bin/env python3
from __sshsetup__ import SSHSetup  # Used to set up Netmiko SSH sessions to specified devices. Needs to be a local import due to the calling method
from __loggersetup__ import Logger  # Sets up logging
from __configsetup__ import ConfigLoad  # Loads the config file
import json  # Parse the input device info
import time  # For sleep functions

# The following items are for processing the configuration file
import os  # Assists with filesystem items
# The following items are for setting up file and console logging
import sys  # Script input call

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file

BAPDID = sys.argv[1]  # Used for logging purposes
ReloadTime = int(sys.argv[2])  # Time to wait for the reload
TargetJSON = sys.argv[3]  # Device info in a JSON format
TargetInfo = json.loads(TargetJSON)
NetmikoProfile = sys.argv[4]
TargetFile = sys.argv[5]  # Verification of the proper image being applied

OutputLog.error(f'DID {BAPDID}- waiting {ReloadTime} seconds before proceeding')
time.sleep(ReloadTime)

# Create the connection and run the relevant command if the connection established properly
OutputLog.info(f'DID {BAPDID}- creating SSH session using profile {NetmikoProfile} to start the upgrade')
device_connection, postloginbanner = SSHSetup(TargetInfo, NetmikoProfile)

# Navigate to the relevant menu
device_connection.send_command("system", expect_string=r'System>')
device_connection.send_command("firmware", expect_string=r'firmware>')

# Start the install and check for a proper response
installresult = device_connection.send_command_timing("install")
if "ERROR, cannot proceed: no valid package" in installresult:
    OutputLog.error(f'DID {BAPDID}- failed to find the package!')
    device_connection.disconnect()
elif "is incompatable with hardware" in installresult:  # Typo is intentional- this is how the Nomadix displays it
    OutputLog.error(f'DID {BAPDID}- package is incompatible!')
    device_connection.disconnect()
elif "invalid checksum in" in installresult:
    OutputLog.error(f'DID {BAPDID}- package has an incorrect checksum!')
    device_connection.disconnect()
elif TargetFile in installresult:
    device_connection.send_command("yes", expect_string=r'Please wait while the new image is being extracted')
    OutputLog.error(f'DID {BAPDID}- device is rebooting for the upgrade')
    time.sleep(30)
    device_connection.disconnect()
else:
    device_connection.send_command_timing("no")
    OutputLog.error(f'DID {BAPDID}- unknown state- cancelled the install!')
    device_connection.disconnect()
