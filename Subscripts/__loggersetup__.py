#!/usr/bin/env python3
# The following items are for setting up file and console logging
import logging  # Module for logging to console/file
import logging.config  # Loads config from a file
import os  # Assists with filesystem items


def Logger(ScriptName):

    log_file_dir = os.path.split(os.path.realpath(os.path.expanduser('~/.loggingconfig.conf')))[
        0]  # This will give us the directory of the config file as the logger can't handle relative paths
    logging.config.fileConfig(os.path.join(log_file_dir, '.loggingconfig.conf'), disable_existing_loggers=False)  # This specifies the config file location
    OutputLog = logging.getLogger(ScriptName)  # Define the logger based on the script name
    return OutputLog

