#!/usr/bin/env python3
from __sshsetup__ import SSHSetup  # Used to set up Netmiko SSH sessions to specified devices. Needs to be a local import due to the calling method
from __loggersetup__ import Logger  # Sets up logging
from __configsetup__ import ConfigLoad  # Loads the config file
import json  # Parse the input device info
import time  # For sleep functions

# The following items are for processing the configuration file
import os  # Assists with filesystem items
# The following items are for setting up file and console logging
import sys  # Script input call

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file

BAPDID = sys.argv[1]  # Used for logging purposes
ReloadTime = int(sys.argv[2])  # Time to wait for the reload
TargetJSON = sys.argv[3]  # Device info in a JSON format
TargetInfo = json.loads(TargetJSON)
NetmikoProfile = sys.argv[4]

OutputLog.error(f'DID {BAPDID}- waiting {ReloadTime} seconds before proceeding with a reboot')
time.sleep(ReloadTime)

# Create the connection and reboot the device
OutputLog.info(f'DID {BAPDID}- creating SSH session using profile {NetmikoProfile} to start the reboot')
device_connection, postloginbanner = SSHSetup(TargetInfo, NetmikoProfile)
device_connection.send_command_timing('reboot')
time.sleep(30)
device_connection.disconnect()

OutputLog.error(f'DID {BAPDID}- sent reboot command- waiting 10 minutes before proceeding')
