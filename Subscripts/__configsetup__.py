#!/usr/bin/env python3
import configparser  # ConfigParser allows us to have variables in a static file
import base64  # Base64 will allow decoding the password in the config file
import os  # Assists with filesystem items


def ConfigLoad():
    # Load the config file
    try:
        configuration = configparser.ConfigParser()
        configuration.read(os.path.expanduser('~/.scriptconfig.txt'))

        # Define the configuration file items in a dict
        ConfigFile = {'ClientID': configuration.get("configuration", "ClientID"),
                      'AuthToken': configuration.get("configuration", "AuthToken"),
                      'StandardUsername': configuration.get("configuration", "StandardUsername"),
                      'StandardPassword': base64.b64decode(configuration.get("configuration", "StandardPassword")).decode("utf-8"),
                      'TACACSUsername': configuration.get("configuration", "TACACSUsername"),
                      'TACACSPassword': base64.b64decode(configuration.get("configuration", "TACACSPassword")).decode("utf-8")}
    except KeyError:
        print("Unable to load the configuration file- please verify it exists!")
        ConfigFile = "Failed"
        quit()

    return ConfigFile
