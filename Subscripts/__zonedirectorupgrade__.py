#!/usr/bin/env python3
from __sshsetup__ import SSHSetup  # Used to set up Netmiko SSH sessions to specified devices. Needs to be a local import due to the calling method
from __loggersetup__ import Logger  # Sets up logging
from __configsetup__ import ConfigLoad  # Loads the config file
import json  # Parse the input device info
import time  # For sleep functions

# The following items are for processing the configuration file
import os  # Assists with filesystem items
# The following items are for setting up file and console logging
import sys  # Script input call

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file

BAPDID = sys.argv[1]  # Used for logging purposes
ReloadTime = int(sys.argv[2])  # Time to wait for the reload
TargetJSON = sys.argv[3]  # Device info in a JSON format
TargetInfo = json.loads(TargetJSON)
NetmikoProfile = sys.argv[4]
UpgradeJSON = sys.argv[5]
UpgradeInfo = json.loads(UpgradeJSON)


def AbortNeeded(Reason):
    import os
    OutputLog.error(f'DID {BAPDID}- {Reason} - cannot proceed and canceling further upgrades for this device!')
    os.system(f"pgrep -f '\"bapdid\": {BAPDID}'|xargs kill -9")
    quit()


OutputLog.error(f'DID {BAPDID}- waiting {ReloadTime} seconds before proceeding with an upgrade to {UpgradeInfo["FileName"]}')
time.sleep(ReloadTime)

# Create the connection and upgrade the firmware
try:
    OutputLog.info(f'DID {BAPDID}- creating SSH session using profile {NetmikoProfile} to start the firmware upgrade')
    device_connection, postloginbanner = SSHSetup(TargetInfo, NetmikoProfile)

    # Get the current version to make sure the unit is on the version we expect
    currentversion = device_connection.send_command('show sysinfo')

    if UpgradeInfo['OldVersion'] not in currentversion:
        AbortNeeded(f'device is not on the expected version to start- {UpgradeInfo["OldVersion"]} expected')

    # Navigate to the menu
    device_connection.send_command('config', expect_string=r'(config)')
    device_connection.send_command('system', expect_string=r'(config-sys)')

    device_connection.send_command('support-entitle')
    supportcheck = device_connection.send_command('show support')

    if 'inactive' in supportcheck:
        device_connection.disconnect()
        AbortNeeded('device cannot download support info')

    # Go back to the base menu
    device_connection.send_command('quit', expect_string=r'(config)')
    device_connection.send_command('quit', expect_string=r'ruckus#')

    # Start the upgrade
    device_connection.send_command('debug', expect_string=r'(debug)')
    upgraderesult = device_connection.send_command_timing(f"fw_upgrade -p ftp -s {UpgradeInfo['FTPServerUsername']}:{UpgradeInfo['FTPServerPassword']}@{UpgradeInfo['FTPServerAddress']} -n /home/support{UpgradeInfo['FilePath']}{UpgradeInfo['FileName']}")

    # Check the upgrade result to determine state
    if 'Are you sure you want to upgrade the entire wireless network?' in upgraderesult:
        device_connection.write_channel(b'y')
        time.sleep(30)
        device_connection.disconnect()
        OutputLog.error(f'DID {BAPDID}- started upgrade from {UpgradeInfo["OldVersion"]} using {UpgradeInfo["FileName"]}')
    elif 'The new firmware does not support the following APs' in upgraderesult:
        device_connection.write_channel(b'1')
        device_connection.disconnect()
        AbortNeeded(f'APs not supported on {UpgradeInfo["FileName"]}')
    elif 'Upgrade failed' in upgraderesult:
        device_connection.disconnect()
        AbortNeeded(f'failed to download {UpgradeInfo["FileName"]} - please check this unit')
    elif 'Out of memory' in upgraderesult:
        device_connection.disconnect()
        AbortNeeded(f'out of memory even after reboot - please check this unit')
    else:
        device_connection.disconnect()
        OutputLog.debug(f'DID {BAPDID}- {upgraderesult}')
        AbortNeeded(f'unable to upgrade using {UpgradeInfo["FileName"]} - check the debug logs')
except:
    AbortNeeded('Unable to issue SSH commands correctly- device may need to be done manually')
