#!/usr/bin/env python3

if __package__:  # As this script is used for the one-off reboots we may need to alter how the packages are imported
    from .__loggersetup__ import Logger  # Sets up logging
    from .__configsetup__ import ConfigLoad  # Loads the config file
else:
    from __loggersetup__ import Logger  # Sets up logging
    from __configsetup__ import ConfigLoad  # Loads the config file

from netmiko import ConnectHandler  # Handles SSH connections to devices
from netmiko.ssh_exception import AuthenticationException, \
    NetMikoTimeoutException  # SSH Error handling for incorrect credentials and timeouts
from netmiko.ssh_autodetect import SSHDetect  # Automatic connection type determination
from paramiko.ssh_exception import SSHException
import time  # For sleep functions

# The following items are for processing the configuration file
import os  # Assists with filesystem items
# The following items are for setting up file and console logging
import sys  # Script input call

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


def SSHSetup(TargetInfo, NetmikoProfile, global_delay=6):  # This creates the connection based on information provided, and retries for TACACS and standard credentials

    BAPDID = str(TargetInfo['bapdid'])  # Defined for log reference

    OutputLog.debug(f'{BAPDID} Connecting to {NetmikoProfile} at {time.ctime()}')

    # The banner will be provided back to the function call in case it is needed
    postloginbanner = ""

    # Define the connection info using the connection type provided
    connection_info = {'device_type': NetmikoProfile, 'ip': TargetInfo['ipaddress'],
                       'username': TargetInfo['username'], 'password': TargetInfo['password'],
                       'port': TargetInfo['adminport'], 'global_delay_factor': global_delay, 'banner_timeout': 60, 'blocking_timeout': 20, 'secret': TargetInfo['password']}

    for attempt in range(4):  # Attempt to connect to the device. This is done in a loop to retry with other credentials

        if 'autodetect' in NetmikoProfile:  # This uses the same authentication flow, but a different module
            try:
                sshdetection = SSHDetect(**connection_info)
                match_connection = sshdetection.autodetect()
            except (NetMikoTimeoutException, ValueError):  # Catch SSH connection errors
                OutputLog.error(f'DID {BAPDID}- Unable to connect to the device- timeout!')
                return False, False
            except (AuthenticationException, OSError):  # Catch incorrect credentials. Based on the attempt, set the credentials and try again with continue
                if attempt == 1:
                    OutputLog.debug(f'DID {BAPDID}- Unable to connect to the device with the BAP credentials- retrying with TACACS')
                    connection_info['username'] = ConfigFile['TACACSUsername']
                    connection_info['password'] = ConfigFile['TACACSPassword']
                    connection_info['secret'] = ConfigFile['TACACSPassword']
                elif attempt == 2:
                    OutputLog.debug(f'DID {BAPDID}- Unable to connect to the device with TACACS credentials- retrying with standard credentials!')
                    connection_info['username'] = ConfigFile['StandardUsername']
                    connection_info['password'] = ConfigFile['StandardPassword']
                    connection_info['secret'] = ConfigFile['StandardPassword']
                elif attempt == 3:  # At this point, we've exhausted our list of credentials. Time to stop the attempts
                    OutputLog.error(f'DID {BAPDID}- Unable to connect to the device with all credentials!')
                    return False, False
                continue
            except SSHException as ConnectError:  # Catch SSH connection errors
                OutputLog.error(f'DID {BAPDID}- Unable to connect to the device- {ConnectError}!')
                return False, False
            except:
                OutputLog.error(f'DID {BAPDID}- Unhandled error!')
                return False, False

            sshdetection.connection.disconnect()
            device_connection = {'device_type': match_connection, 'username': TargetInfo['username'], 'password': TargetInfo['password']}
            break  # If successful, break to escape the loop
        else:  # Start the connection to the device. We need to wait for the connection to properly establish before we proceed
            try:
                device_connection = ConnectHandler(**connection_info)
                time.sleep(1)

                # Some devices have a post-connection login attempt, such as Ruckus ZoneDirectors. We need to grab the channel to make sure we aren't sitting at this prompt
                postloginbanner = device_connection.read_channel()

            except (NetMikoTimeoutException, ValueError):  # Catch SSH connection errors
                OutputLog.error(f'DID {BAPDID}- Unable to connect to the device- timeout!')
                return False, False
            except (AuthenticationException, OSError):  # Catch incorrect credentials. Based on the attempt, set the credentials and try again with continue
                if attempt == 1:
                    OutputLog.debug(f'DID {BAPDID}- Unable to connect to the device with the BAP credentials- retrying with TACACS')
                    connection_info['username'] = ConfigFile['TACACSUsername']
                    connection_info['password'] = ConfigFile['TACACSPassword']
                    connection_info['secret'] = ConfigFile['TACACSPassword']
                elif attempt == 2:
                    OutputLog.debug(f'DID {BAPDID}- Unable to connect to the device with TACACS credentials- retrying with standard credentials!')
                    connection_info['username'] = ConfigFile['StandardUsername']
                    connection_info['password'] = ConfigFile['StandardPassword']
                    connection_info['secret'] = ConfigFile['StandardPassword']
                elif attempt == 3:  # At this point, we've exhausted our list of credentials. Time to stop the attempts
                    OutputLog.error(f'DID {BAPDID}- Unable to connect to the device with all credentials!')
                    return False, False
                continue
            except SSHException as ConnectError:  # Catch SSH connection errors
                OutputLog.error(f'DID {BAPDID}- Unable to connect to the device- {ConnectError}!')
                return False, False
            except:
                OutputLog.error(f'DID {BAPDID}- Unhandled error!')
                return False, False

            if postloginbanner == "":  # Some devices take a few seconds to display relevant info, but we can't wait if we're sitting at a special login prompt
                time.sleep(3)
                postloginbanner = device_connection.read_channel()

            if 'Please login:' in str(postloginbanner):
                device_connection.send_command(TargetInfo['username'], expect_string=r'Password:', max_loops=20)
                device_connection.send_command(TargetInfo['password'], expect_string=r'ruckus>', max_loops=20)
                device_connection.send_command("enable", expect_string=r'ruckus#', max_loops=20)
            elif 'Logon Disclaimer message' in str(postloginbanner):
                postloginbanner = device_connection.send_command('yes', expect_string=r'#', max_loops=20)

            # Make sure the paging display is disabled
            device_connection.disable_paging()
            break  # If successful, break to escape the loop

    return device_connection, postloginbanner


def SSHProfile(TargetInfo):  # This detects the relevant Netmiko profile to use based on Netmiko's autodetection

    BAPDID = str(TargetInfo['bapdid'])  # Defined for log reference
    device_connection, postloginbanner = SSHSetup(TargetInfo, 'autodetect', global_delay=1)

    # Make sure we got a result
    if device_connection:
        pass
    else:
        return False

    OutputLog.debug(f'{BAPDID}- autodetected {device_connection["device_type"]}')

    # If the autodetect could not determine a specific connection type, default to a generic type
    if device_connection["device_type"] is None:
        device_connection["device_type"] = 'generic_termserver'

    return device_connection
