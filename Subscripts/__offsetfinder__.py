#!/usr/bin/env python3
if __package__:  # This script may be used locally, so the imports could change
    from .__loggersetup__ import Logger  # Sets up logging
    from .__configsetup__ import ConfigLoad  # Loads the config file
else:
    from __loggersetup__ import Logger  # Sets up logging
    from __configsetup__ import ConfigLoad  # Loads the config file
# The following packages will allow timezone offset determination from GPS coordinates
from time import localtime
from datetime import datetime, timedelta
from pytz import UnknownTimeZoneError
from pytz import timezone
from timezonefinder import TimezoneFinder

import requests  # Gathering of API data from a URL
import json  # JSON allows for processing of JSON-formatted data
import os  # Assists with filesystem items
import sys  # Script input call

# OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


def TimezoneOffset(HSOID):
    AuthParameters = {"access_token": ConfigFile['AuthToken'], "client_id": ConfigFile['ClientID']}
    HSOInfoJSON = requests.get('https://api.aws.opennetworkexchange.net/api/v2/accounts/{0}'.format(HSOID), params=AuthParameters).json()

    try:
        return str(int(datetime.now(timezone(TimezoneFinder().certain_timezone_at(lat=HSOInfoJSON['latitude'], lng=HSOInfoJSON['longitude']))).utcoffset().total_seconds() / 3600))  # Current time offset at target timezone
    except (AttributeError, UnknownTimeZoneError):
        if HSOInfoJSON['timeZoneOffset'] == int(datetime.now(tz=timezone("America/New_York")).utcoffset().total_seconds() * 1000):  # This is the default value, so list as unconfirmed
            return f"{int(HSOInfoJSON['timeZoneOffset'] / 3600000)}_Unconfirmed"
        else:
            return str(int(HSOInfoJSON['timeZoneOffset'] / 3600000))  # Otherwise, return the start based on the reported offset
