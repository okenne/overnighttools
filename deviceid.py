#!/usr/bin/env python3
from didgather import DIDGather  # DIDgather will allow us to gather prerequisite information from the BAP
from Subscripts import Logger  # Sets up logging
from Subscripts import ConfigLoad  # Loads the config file
from Subscripts import SSHSetup  # Used to set up Netmiko SSH sessions to specified devices
from Subscripts import SSHProfile  # Used to determine applicable Netmiko profile to use
import time  # For sleep functions
import re  # Regular expressions for text operations
from collections import Counter  # Allows easy counting of AP items
from pysnmp.hlapi import getCmd, bulkCmd, nextCmd, SnmpEngine, CommunityData, UdpTransportTarget, ContextData, ObjectType, ObjectIdentity  # SNMP connection items
from operator import itemgetter  # Allows for multiple sort options

# The following are for SSH connection items
from netmiko.ssh_exception import NetMikoTimeoutException  # Error handling for incorrect credentials and timeouts

# The following items are for setting up file and console logging
import sys  # Script input call
import os  # Assists with filesystem items

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


class TriggerException(Exception):  # Present for use when we need to trigger a custom exception clause
    pass


class DeviceID:

    def Handler(self, TargetID, DIDRunOption):

        # Set up some starting items
        self.SNMPGetInfo = 'Initial'
        self.SNMPWalkInfo = 'Initial'

        # Check for pre-run related run options
        if 'hso' in DIDRunOption:
            print(f'{TargetID}- Gathering device info from the API')
            HSOInfo = DIDGather().Handler(TargetID, DIDRunOption)  # Gather the info for the HSO's devices
            print(f'{TargetID}- API data gathered, proceeding with identification')
            HSOID = []  # Create a list to assign the results to

            for DeviceRow in HSOInfo:  # Each row is the API data for a single device

                self.DeviceInfo = DeviceRow  # The row would be equivalent to a single device's output
                self.BAPDID = str(self.DeviceInfo['bapdid'])
                print(f'{self.BAPDID} started at {time.ctime()}')

                # Run the main- SNMP preferred, but CLI first if requested
                if 'cli' in DIDRunOption:
                    self.DeviceInfo['snmp_reply'] = 'Skipped'
                    self.CLIGather()

                else:
                    self.SNMPGather()

                    if self.DeviceInfo['snmp_reply'] == 'Complete':
                        pass
                    else:
                        self.CLIGather()

                HSOID.append(self.DeviceInfo)  # Add the device to the overall results

                # Reset for the next iteration
                del self.DeviceInfo
                del self.BAPDID
                self.SNMPGetInfo = 'Initial'
                self.SNMPWalkInfo = 'Initial'

            return HSOID

        else:
            self.DeviceInfo = DIDGather().Handler(TargetID, DIDRunOption)  # Gather the information for the device
            try:
                self.BAPDID = str(self.DeviceInfo['bapdid'])
            except TypeError:
                OutputLog.info(f'{TargetID} has monitoring disabled and checking these was not requested')
                self.DeviceInfo['bapdid'] = TargetID
                self.DeviceInfo['snmp_reply'] = 'Target_Disabled'
                self.DeviceInfo['ssh_reply'] = 'Target_Disabled'
                return self.DeviceInfo

            if self.DeviceInfo['baplaststatus']:
                pass  # If the device was last status up, we proceed
            else:
                if self.DeviceInfo['bapmonitortype'] == 0:
                    pass  # If monitoring is disabled, we requested this device
                elif 'stat' in DIDRunOption:
                    pass  # If we request to ignore status, continue
                else:
                    OutputLog.error(f'{TargetID} this device is down!')
                    self.DeviceInfo['snmp_reply'] = 'Target_Offline'
                    self.DeviceInfo['ssh_reply'] = 'Target_Offline'
                    return self.DeviceInfo

            OutputLog.debug(f'{self.BAPDID} started at {time.ctime()}')

            # Run the main- SNMP preferred, but CLI first if requested
            if 'cli' in DIDRunOption:
                self.DeviceInfo['snmp_reply'] = 'Skipped'
                self.CLIGather()

            else:
                self.SNMPGather()

                if self.DeviceInfo['snmp_reply'] == 'Complete':
                    pass
                else:
                    self.CLIGather()

            return self.DeviceInfo

    def SNMPGet(self, OIDlist):  # This is to gather specific OIDs, passed as an input list

        OutputLog.debug(f'{self.BAPDID} Getting {OIDlist} at {time.ctime()}')

        del self.SNMPGetInfo  # Clear previous results
        try:  # Set up as a try/except to suppress tracebacks
            errorIndication, errorStatus, errorIndex, self.SNMPGetInfo = next(
                getCmd(SnmpEngine(),
                       CommunityData(self.DeviceInfo['snmpcommunity']),
                       UdpTransportTarget((self.DeviceInfo['ipaddress'], self.DeviceInfo['snmpport']), timeout=1.0,
                                          retries=5),
                       ContextData(),
                       *[ObjectType(ObjectIdentity(oid)) for oid in OIDlist])
            )
            self.DeviceInfo['snmp_reply'] = 'Complete'  # Set a marker to indicate an SNMP response was successful

        except Exception as exceptionError:
            OutputLog.error(f'DID {self.BAPDID}- Could not get the requested OIDs {OIDlist}- see debug logs!')
            OutputLog.debug(errorIndication, errorStatus, errorIndex, exceptionError, self.SNMPGetInfo)
            self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful

            # Check if the results are blank, which would indicate that the request timed out
        if errorStatus == 0:
            OutputLog.debug(f'{self.BAPDID} Got {self.SNMPGetInfo} at {time.ctime()}')

        else:
            OutputLog.debug(f'DID {self.BAPDID} - Improper or failed reply to SNMP request- OID {OIDlist}!')
            self.DeviceInfo['snmp_reply'] = 'None'  # Set a marker to indicate an SNMP response was unsuccessful

    def SNMPBulkWalk(self, OIDlist):  # This is to gather a single SNMP walk, passed as an OID
        OutputLog.debug(f'{self.BAPDID} Walking {OIDlist} at {time.ctime()}')

        del self.SNMPWalkInfo  # Clear previous results
        self.SNMPWalkInfo = []  # Define a blank list to add the items to

        try:  # Set up as a try/except to suppress tracebacks
            for (errorIndication, errorStatus, errorIndex, RawSNMPInfo) in bulkCmd(SnmpEngine(),
                                                                                   CommunityData(self.DeviceInfo['snmpcommunity']),
                                                                                   UdpTransportTarget((self.DeviceInfo['ipaddress'],
                                                                                                       self.DeviceInfo['snmpport']),
                                                                                                      timeout=1.5,
                                                                                                      retries=50),
                                                                                   ContextData(),
                                                                                   0, 3000,
                                                                                   ObjectType(ObjectIdentity(OIDlist)),
                                                                                   lexicographicMode=False,
                                                                                   # This will prevent the walk from continuing out of scope
                                                                                   ignoreNonIncreasingOid=True,
                                                                                   # Some devices will return entries out of order, such as WLCs
                                                                                   lookupMib=False):

                if errorIndication:  # Error out if one of these items are present
                    OutputLog.error(f'DID {self.BAPDID}- Could not walk OID {OIDlist}- {errorIndication}!')
                    self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful
                    return
                elif errorStatus:  # Error out if one of these items are present

                    if str(errorStatus) == 'tooBig':
                        # If the return is too big, we need to use NextCMD instead of BulkCMD to iterate instead of getting as one chunk
                        OutputLog.info(f'{self.BAPDID} - Could not walk the requested OIDs {OIDlist} is too big- trying with nextCMD instead')
                        raise TriggerException

                    OutputLog.error(f'DID {self.BAPDID}- Could not walk the requested OIDs {OIDlist}- {errorStatus}- see debug logs!')
                    self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful
                    OutputLog.debug('%s at %s' % (errorStatus.prettyPrint(),
                                                  errorIndex and RawSNMPInfo[int(errorIndex) - 1][0] or '?'))
                    return
                else:  # Add the results into the previously defined list
                    for SNMPRow in RawSNMPInfo:
                        self.SNMPWalkInfo.append(SNMPRow)

        except TriggerException:
            self.SNMPNextWalk(OIDlist)

        except Exception as exceptionError:
            OutputLog.error(f'DID {self.BAPDID}- Could not get the requested OIDs {OIDlist}, {exceptionError}- see debug logs!')
            OutputLog.debug(RawSNMPInfo)
            self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful

        OutputLog.debug(f'{self.BAPDID} Got {self.SNMPGetInfo} at {time.ctime()}')

    def SNMPNextWalk(self, OIDlist):  # This is to gather a single SNMP walk, passed as an OID- some OIDs have issues with the getBulk option

        OutputLog.debug(f'{self.BAPDID} NextCMD Walking {OIDlist} at {time.ctime()}')

        del self.SNMPWalkInfo  # Clear previous results
        self.SNMPWalkInfo = []  # Define a blank list to add the items to

        try:  # Set up as a try/except to suppress tracebacks
            for (errorIndication, errorStatus, errorIndex, RawSNMPInfo) in nextCmd(SnmpEngine(),
                                                                                   CommunityData(self.DeviceInfo['snmpcommunity']),
                                                                                   UdpTransportTarget((self.DeviceInfo['ipaddress'],
                                                                                                       self.DeviceInfo['snmpport']),
                                                                                                      timeout=1.5,
                                                                                                      retries=50),
                                                                                   ContextData(),
                                                                                   ObjectType(ObjectIdentity(OIDlist)),
                                                                                   lexicographicMode=False,
                                                                                   # This will prevent the walk from continuing out of scope
                                                                                   ignoreNonIncreasingOid=True,
                                                                                   # Some devices will return entries out of order, such as WLCs
                                                                                   lookupMib=False):

                if errorIndication:  # Error out if one of these items are present
                    OutputLog.error(f'DID {self.BAPDID}- Could not walk OID {OIDlist}- {errorIndication}!')
                    self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful
                    return
                elif errorStatus:  # Error out if one of these items are present

                    if str(errorStatus) == 'tooBig':
                        OutputLog.error(f'DID {self.BAPDID}- Could not walk the requested OIDs {OIDlist} is too big- trying with nextCMD instead!')

                    OutputLog.error(f'DID {self.BAPDID}- Could not walk the requested OIDs {OIDlist}- {errorStatus}- see debug logs!')
                    self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful
                    OutputLog.debug('%s at %s' % (errorStatus.prettyPrint(),
                                                  errorIndex and RawSNMPInfo[int(errorIndex) - 1][0] or '?'))
                    return
                else:  # Add the results into the previously defined list
                    for SNMPRow in RawSNMPInfo:
                        self.SNMPWalkInfo.append(SNMPRow)

        except Exception as exceptionError:
            OutputLog.error(f'DID {self.BAPDID}- Could not get the requested OIDs {OIDlist}, {exceptionError}- see debug logs!')
            OutputLog.debug(RawSNMPInfo)
            self.DeviceInfo['snmp_reply'] = 'Incomplete'  # Set a marker to indicate an SNMP response was unsuccessful

        OutputLog.debug(f'{self.BAPDID} Got {self.SNMPGetInfo} at {time.ctime()}')

    def SNMPGather(self):  # This is the main function- SNMP is priority

        self.DeviceInfo['ssh_reply'] = 'Skipped'

        # Get some initial information to determine the vendor and generic hostname
        OIDlist = ('1.3.6.1.2.1.1.2.0',  # Vendor- returns an OID resembling "1.3.6.1.4.1.1.1.1"
                   '1.3.6.1.2.1.1.5.0')  # Hostname- returns a string resembling "ABCDESW01-01-MDF"
        OutputLog.debug(f'DID {self.BAPDID}- Gathering vendor and hostname')
        self.SNMPGet(OIDlist)

        if self.DeviceInfo['snmp_reply'] == 'Complete':  # If the device didn't reply to the initial items, skip further processing
            pass
        else:
            return

        # Split the output into the relevant variables- the results are passed as [X][Y], where X is the individual OID get and Y is the sectioned output (0 as the query, 1 as the output)
        # This is run as a try as it is the first SNMP request called
        try:
            self.DeviceInfo['vendor'] = str(self.SNMPGetInfo[0][1])
            self.DeviceInfo['hostname'] = str(self.SNMPGetInfo[1][1])
            self.DeviceInfo['modules'] = {}
        except IndexError:
            OutputLog.error(f'DID {self.BAPDID}- Improper or failed reply to SNMP request- OID {OIDlist}!')
            OutputLog.debug(f'DID {self.BAPDID}- {self.SNMPGetInfo}')
            self.DeviceInfo['snmp_reply'] = 'None'  # Set a marker to indicate an SNMP response was unsuccessful
            return

        # This logic is based off of the IANA private enterprise numbers as assigned here: https://www.iana.org/assignments/enterprise-numbers/enterprise-numbers
        if '1.3.6.1.4.1.9.' in self.DeviceInfo['vendor']:  # This is Cisco's ID
            self.DeviceInfo['vendor'] = 'Cisco'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Cisco device')

            # Need to walk the model, as stacks and WLCs are going to report multiple models
            OIDlist = '1.3.6.1.2.1.47.1.1.1.1.13'  # This is Cisco's model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPBulkWalk(OIDlist)

            # Cisco devices are fairly consistent across models, so we can treat a number the same
            RegexMatch = 'AIR-CT[0-9]{4}|ASA[0-9]{4}|ASR[0-9]{4}|C[0-9]{4}-SPE|CISCO[0-9]{4}|WS-C[0-9]{4}|WS-.*SUP'  # We'll define the list here as it is used twice
            if re.search(RegexMatch, str(self.SNMPWalkInfo)) is not None:
                # We need to loop through the models given to filter out blank or unrelated entries
                for ModelRow in range(len(self.SNMPWalkInfo)):
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Most lines will be blank
                        if re.search(RegexMatch, str(Model)) is not None:  # Some lines are models of SFPs or other hardware not relevant to our interests

                            # Set the entry reference to use for further information
                            # Split on "." and only get the last item, for instance 1.3.6.1.2.1.47.1.1.1.1.13.1000 would become 1000
                            ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split('.').pop()

                            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(Model)):
                                if ModuleRef == '1':
                                    ModuleEntry = str('chassis')  # This is used for variable references
                                else:
                                    ModuleEntry = ModuleRef[0]  # This is used for variable references- shortened to ensure consistency
                            else:
                                ModuleEntry = ModuleRef  # This is used for variable references

                            # Define the OID list
                            OIDlist = ('1.3.6.1.2.1.47.1.1.1.1.8.' + ModuleRef,  # Hardware revision- returns a string resembling "V01"
                                       '1.3.6.1.2.1.47.1.1.1.1.10.' + ModuleRef,  # Software version- returns a string resembling "15.2(4)E8"
                                       '1.3.6.1.2.1.47.1.1.1.1.11.' + ModuleRef)  # Serial- returns a string resembling "AAA1111A1A1"
                            OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                            self.SNMPGet(OIDlist)

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                            # Add the results to a sub-dict in the device dict
                            self.DeviceInfo['modules'][ModuleEntry]['model'] = str(Model)
                            if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['hardwarerev'] = str(self.SNMPGetInfo[0][1])
                            if str(self.SNMPGetInfo[1][1]) != "":
                                self.DeviceInfo['modules'][ModuleEntry]['version'] = re.sub('[a-zA-Z]{3,} ', '', str(self.SNMPGetInfo[1][1]))  # Subbed in order to strip software names from 16.x versions
                                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                            if str(self.SNMPGetInfo[2][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[2][1])

                            # Grab the AP models to add to a list
                        elif re.search('AIR-CAP[0-9]{3,4}|AIR-LAP[0-9]{3,4}', str(Model)) is not None:
                            try:
                                APModels  # See if the var exists first
                            except NameError:
                                APModels = []  # If not, define it

                            APModels.extend([str(Model)])  # Add the AP to the list

                    # There's some devices that have need some post-processing- we'll handle them here
                if re.search('WS-C3650|WS-C3850', str(self.SNMPWalkInfo)) is not None:  # There is a fake model number for the stack, so we will need to skip the first module entry
                    del self.DeviceInfo['modules']['chassis']

                if re.search('ASA[0-9]{4}', str(self.SNMPWalkInfo)) is not None:  # Check for failover
                    OIDlist = ('1.3.6.1.4.1.9.9.147.1.2.1.1.1.4.6',  # This will provide the failover status for the current unit- either "Failover Off" or "Active unit"/"Standby unit"
                               '1.3.6.1.4.1.9.9.147.1.2.1.1.1.4.7')  # Standby gathered for future use
                    self.SNMPGet(OIDlist)

                    if re.search('Active unit|Standby unit', str(self.SNMPGetInfo)) is not None:
                        self.DeviceInfo['failover'] = True
                        self.DeviceInfo['peeripaddress'] = 'Unknown'
                        self.DeviceInfo['snmp_reply'] = 'CLI_Required'  # Cisco doesn't display as much about the secondary unit in SNMP, so we will need to proceed to CLI
                        OutputLog.debug(f'DID {self.BAPDID}- Unable to get full info in SNMP, falling back to CLI')
                    else:
                        self.DeviceInfo['failover'] = False

                try:  # If the AP model list is present, let's aggregate it
                    ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                    for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                        self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
                except NameError:
                    pass

        elif '1.3.6.1.4.1.11.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Hewlett-Packard'
            OutputLog.debug(f'DID {self.BAPDID}- Processing HP device')

            # Need to walk the model, as stacks and WLCs are going to report multiple models
            OIDlist = '1.3.6.1.2.1.47.1.1.1.1.2'  # This is the generic ENTITY-MIB, which serves as HP's model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPBulkWalk(OIDlist)

            # HP switches will show as either "J# # X Switch # # " or "J# # X # #  Switch", such as "HP J8697A Switch 5406zl"
            RegexMatch = 'J[L0-9]{4}.*[0-9]{4}.*Switch|J[L0-9]{4}.*Switch.*[0-9]{4}'  # We'll define the list here as it is used twice
            if re.search(RegexMatch, str(self.SNMPWalkInfo)) is not None:
                # We need to loop through the models given to filter out blank or unrelated entries
                for ModelRow in range(len(self.SNMPWalkInfo)):
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Most lines will be blank
                        if re.search(RegexMatch, str(Model)) is not None:  # Some lines are models of SFPs or other hardware not relevant to our interests

                            # Set the entry reference to use for further information
                            # Split on "." and only get the last item, for instance 1.3.6.1.2.1.47.1.1.1.1.13.1000 would become 1000
                            ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split('.').pop()
                            ModuleEntry = ModuleRef[0]  # This is used for variable references- we only want the first character to ensure consistency with CLI options

                            # Define the OID list
                            OIDlist = ('1.3.6.1.2.1.47.1.1.1.1.10.' + ModuleRef,  # Software version- returns a string resembling "K.16.02.0022m"
                                       '1.3.6.1.2.1.47.1.1.1.1.11.' + ModuleRef)  # Serial- returns a string resembling "AA111AA111"
                            OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                            self.SNMPGet(OIDlist)

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                            # Add the results to a sub-dict in the device dict
                            ModelLine = re.search(' [E]?[0-9]{4}[0-9A-Za-z-]*', str(Model))
                            if ModelLine is not None:
                                self.DeviceInfo['modules'][ModuleEntry]['model'] = str(ModelLine.group().replace(" ", ""))
                            if str(self.SNMPGetInfo[0][1]) != "":
                                self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[0][1])
                                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                            if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[1][1])

        elif '1.3.6.1.4.1.388.' in self.DeviceInfo['vendor']:
            # This is more than likely an Extreme controller, but the actual OID belongs to Symbol (later owned by Motorola)
            self.DeviceInfo['vendor'] = 'Motorola/Extreme'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Motorola/Extreme device')

            # Need to walk the model, as stacks and WLCs are going to report multiple models
            OIDlist = '1.3.6.1.4.1.388.50.1.4.2.4.1.1.1'  # This is Symbol's model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            OutputLog.error(f'DID {self.BAPDID}- Extreme WLCs may take a long time to complete, please wait')
            self.SNMPBulkWalk(OIDlist)

            # Controller names are defined here
            RegexMatch = 'NX[0-9]{4}|RFS[0-9]{4}'  # We'll define the list here as it is used twice
            if re.search(RegexMatch, str(self.SNMPWalkInfo)) is not None:
                # We need to loop through the models given to filter out blank or unrelated entries
                for ModelRow in range(len(self.SNMPWalkInfo)):
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Most lines will be blank
                        if re.search(RegexMatch, str(Model)) is not None:  # Some lines are models of SFPs or other hardware not relevant to our interests

                            # Set the entry reference to use for further information
                            ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split("1.3.6.1.4.1.388.50.1.4.2.4.1.1.1.", 1)[1]
                            # This is used for variable references- shortened because the full reference is quite long
                            ModuleEntry = ModuleRef.split('.').pop()

                            # Define the OID list
                            OIDlist = ('1.3.6.1.4.1.388.50.1.4.2.4.1.1.2.' + ModuleRef,
                                       # Serial- returns a string resembling "11111111111111"
                                       '1.3.6.1.4.1.388.50.1.4.2.4.1.1.3.' + ModuleRef)  # Software version- returns a string resembling "5.9.3.1-005R"
                            OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                            self.SNMPGet(OIDlist)

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                            # Add the results to a sub-dict in the device dict
                            self.DeviceInfo['modules'][ModuleEntry]['model'] = str(Model)
                            if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[0][1])
                            if str(self.SNMPGetInfo[1][1]) != "":
                                self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[1][1])
                                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']

                            # Grab the AP models to add to a list
                        elif re.search('AP[0-9]{3,4}', str(Model)) is not None:
                            try:
                                APModels  # See if the var exists first
                            except NameError:
                                APModels = []  # If not, define it
                            APModels.extend([str(Model)])  # Add the AP to the list

                try:  # If the AP model list is present, let's aggregate it
                    ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                    for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                        self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
                except NameError:
                    pass

        elif '1.3.6.1.4.1.1916.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Extreme'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Extreme device')

            # Need to walk the model, as stacks and WLCs are going to report multiple models
            OIDlist = '1.3.6.1.2.1.47.1.1.1.1.2'  # This is the generic ENTITY-MIB, which serves as Extreme's model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPBulkWalk(OIDlist)

            # Extreme switches will show as "X460G2"
            RegexMatch = 'X[0-9]{3}'  # We'll define the list here as it is used twice
            if re.search(RegexMatch, str(self.SNMPWalkInfo)) is not None:  # Make sure we got usable data
                # We need to loop through the models given to filter out blank or unrelated entries
                PreviousRow = ""  # The model is the line after the slot number, so we need to keep track of the previous line in case we need to match a model to its slot
                for ModelRow in range(1, len(self.SNMPWalkInfo)):  # Start at line 2 as the first line is a false entry for the stack
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Most lines will be blank
                        if re.search(RegexMatch, str(Model)) is not None:  # Some lines are models of SFPs or other hardware not relevant to our interests
                            # Set the entry reference to use for further information
                            # Split on "." and only get the last item, for instance 1.3.6.1.2.1.47.1.1.1.1.13.1000 would become 1000
                            ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split('.').pop()
                            ModuleEntry = PreviousRow.replace("Slot-", "")  # This is used for variable references

                            # Define the OID list
                            OIDlist = ('1.3.6.1.2.1.47.1.1.1.1.10.' + ModuleRef,
                                       # Software version- returns a string resembling "22.3.1.4"
                                       '1.3.6.1.2.1.47.1.1.1.1.11.' + ModuleRef)  # Serial- returns a string resembling "1111A-11111"
                            OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                            self.SNMPGet(OIDlist)

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                            # Add the results to a sub-dict in the device dict
                            self.DeviceInfo['modules'][ModuleEntry]['model'] = str(Model)
                            if str(self.SNMPGetInfo[0][1]) != "":
                                self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[0][1])
                                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                            if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[1][1])
                        else:
                            PreviousRow = str(Model)

        elif '1.3.6.1.4.1.1991.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Brocade/Ruckus'  # This is more than likely a Ruckus switch, but the actual OID belongs to Brocade
            OutputLog.debug(f'DID {self.BAPDID}- Processing Brocade/Ruckus device')

            # Need to walk the model, as stacks are going to report multiple models
            OIDlist = '1.3.6.1.4.1.1991.1.1.3.31.2.1.1.5'  # This is Brocade's model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPBulkWalk(OIDlist)

            # We need to loop through the models given to filter out blank or unrelated entries
            for ModelRow in range(len(self.SNMPWalkInfo)):
                Model = self.SNMPWalkInfo[ModelRow][1]
                if str(Model) != "":  # Most lines will be blank

                    # Set the entry reference to use for further information
                    # Split on "." and only get the last item, for instance 1.3.6.1.2.1.47.1.1.1.1.13.1000 would become 1000
                    ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split('.').pop()
                    ModuleEntry = ModuleRef  # This is used for variable references

                    # Define the OID list
                    OIDlist = ('1.3.6.1.4.1.1991.1.1.2.8.2.1.7.' + ModuleRef + '.1',
                               # Serial- returns a string resembling "AAA1111A111"
                               '1.3.6.1.4.1.1991.1.1.3.31.2.2.1.13.' + ModuleRef)  # Software version- returns a string resembling "11.1.11aA111"
                    OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                    self.SNMPGet(OIDlist)

                    # Let's instantiate a dict within the device dict for module entry
                    self.DeviceInfo['modules'][ModuleEntry] = {}

                    # Add the results to a sub-dict in the device dict
                    self.DeviceInfo['modules'][ModuleEntry]['model'] = str(Model)
                    if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[0][1])
                    if str(self.SNMPGetInfo[1][1]) != "":
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[1][1])
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']

        elif '1.3.6.1.4.1.3097.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Watchguard'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Watchguard device')

            # Define the OID list
            OIDlist = ('1.3.6.1.2.1.1.1.0',  # Model- returns a string resembling "M570"
                       '1.3.6.1.4.1.3097.6.3.1.0',  # Version- returns a string resembling "<sysa:12.5.B597646>  <sysb:12.3.1B585922>"
                       '1.3.6.1.4.1.3097.6.6.1.0',  # Cluster configuration status- returns an integer of 0 (disabled) or 1 (enabled)- any later items will be null if disabled
                       '1.3.6.1.4.1.3097.6.6.2.0',  # First member ID (serial)- returns a string resembling "112233445566"
                       '1.3.6.1.4.1.3097.6.6.3.0',  # First member status- returns an integer- values are disabled(0), worker(1), backup(2), master(3), idle(4), standby(5)
                       '1.3.6.1.4.1.3097.6.6.8.0',  # Second member ID (serial)- returns a string resembling "112233445566"
                       '1.3.6.1.4.1.3097.6.6.9.0')  # Second member status- returns an integer- values are disabled(0), worker(1), backup(2), master(3), idle(4), standby(5)
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPGet(OIDlist)

            ModuleEntry = str('1')  # This is used for variable references- the first is always present

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Add the results to a sub-dict in the device dict
            if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[0][1])
            if str(self.SNMPGetInfo[1][1]) != "":
                self.DeviceInfo['modules'][ModuleEntry]['version'] = re.search('(?<=<sysa:)[0-9A-Z.]*', str(self.SNMPGetInfo[1][1])).group()
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            if str(self.SNMPGetInfo[4][1]) != "":
                # This is an integer response, but is more useful interpreted
                if str(self.SNMPGetInfo[4][1]) == '0':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'disabled'
                elif str(self.SNMPGetInfo[4][1]) == '1':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'worker'
                elif str(self.SNMPGetInfo[4][1]) == '2':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'Backupmaster'
                elif str(self.SNMPGetInfo[4][1]) == '3':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'Master'
                elif str(self.SNMPGetInfo[4][1]) == '4':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'idle'
                elif str(self.SNMPGetInfo[4][1]) == '5':
                    self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'standby'

            if str(self.SNMPGetInfo[2][1]) == "1":  # Check to see if clustering is enabled
                if str(self.SNMPGetInfo[3][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[3][1])

                SecondModuleEntry = str('2')  # This is used for variable references

                # Let's instantiate a dict within the device dict for module entry
                self.DeviceInfo['modules'][SecondModuleEntry] = {}

                # Add the info to the device dict
                if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][SecondModuleEntry]['model'] = str(self.SNMPGetInfo[0][1])
                if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][SecondModuleEntry]['version'] = re.search('(?<=<sysa:)[0-9A-Z.]*', str(self.SNMPGetInfo[1][1])).group()
                if str(self.SNMPGetInfo[5][1]) != "": self.DeviceInfo['modules'][SecondModuleEntry]['serial'] = str(self.SNMPGetInfo[5][1])

                if str(self.SNMPGetInfo[6][1]) != "":
                    # This is an integer response, but is more useful interpreted
                    if str(self.SNMPGetInfo[6][1]) == '0':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'disabled'
                    elif str(self.SNMPGetInfo[6][1]) == '1':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'worker'
                    elif str(self.SNMPGetInfo[6][1]) == '2':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'Backupmaster'
                    elif str(self.SNMPGetInfo[6][1]) == '3':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'Master'
                    elif str(self.SNMPGetInfo[6][1]) == '4':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'idle'
                    elif str(self.SNMPGetInfo[6][1]) == '5':
                        self.DeviceInfo['modules'][SecondModuleEntry]['cluster_status'] = 'standby'

            else:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = 'Not_Supported'

        elif '1.3.6.1.4.1.3309.' in self.DeviceInfo['vendor']:
            OutputLog.debug(f'DID {self.BAPDID}- Processing Nomadix device')

            # Nomadix splits their OIDs based on model- 1.3.6.1.4.1.3309.1.4 for AG5XXX and 1.3.6.1.4.1.3309.1.5 for AG2XXX/3XXX.  The enterprise number reflects this model split, so we will identify which OIDs we need to use based on that

            # Define the OID list
            OIDlist = (self.DeviceInfo['vendor'] + '.35.10.0',  # Model and software version- returns a string resembling "AG 2400 v8.12.018"
                       self.DeviceInfo['vendor'] + '.35.11.0')  # NSE ID- returns a string resembling "11A1AA"
            OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
            self.SNMPGet(OIDlist)

            ModuleEntry = '1'  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # As Nomadix presents the model and version as one string (eg "AG 2400 v8.12.018"), we will need to parse this
            try:
                if str(self.SNMPGetInfo[0][1]) != "":
                    ModelVersion = str(self.SNMPGetInfo[0][1]).split()  # Split into a list on spaces
                    self.DeviceInfo['modules'][ModuleEntry]['model'] = ModelVersion[0] + ModelVersion[1]  # Join AG and the model number, then assign to the device dict
                    self.DeviceInfo['modules'][ModuleEntry]['version'] = ModelVersion[2]  # Assign the version to the device dict
                    self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                else:
                    raise IndexError
            except IndexError:
                OutputLog.error(f'DID {self.BAPDID} did not reply with a usable model- here is what we got- {self.SNMPGetInfo[0][1]}')
                return

            # The NSE ID is functionally equivalent to the serial
            if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[1][1])

            # TEMPORARILY REMOVED DUE TO ISSUESGet failover status- will return null if not licensed
            # OIDlist = (self.DeviceInfo['vendor'] + '.72.1.0',  # Failover state- returns an integer- 0 is disabled, 1 is enabled
            #            self.DeviceInfo['vendor'] + '.72.4.0')  # Failover status- returns an integer- 0 is primary, 1 is secondary and 2 is unknown
            # OutputLog.debug(f'DID {self.BAPDID}- Gathering failover info')
            # self.SNMPGet(OIDlist)

            # Failover status is an integer response, but is more useful interpreted
            # if str(self.SNMPGetInfo[0][1]) == '1':
            #     self.DeviceInfo['failover'] = True  # There is no method to determine the status of a peer, so we must assume it is present
            # else:
            #     self.DeviceInfo['failover'] = False  # There is no method to determine the status of a peer, so we must assume it is present

            # Defined here as this was used prior
            self.DeviceInfo['vendor'] = 'Nomadix'

        elif '1.3.6.1.4.1.8744.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Colubris'
            OutputLog.debug(f'DID {self.BAPDID}- Processing MSM device')

            # Define the OID list
            OIDlist = ('1.3.6.1.4.1.8744.5.6.1.1.1.0',  # Model- returns a string resembling "MSM760"
                       '1.3.6.1.4.1.8744.5.6.1.1.2.0',  # Version- returns a string resembling "6.5.1.0-21733"
                       '1.3.6.1.4.1.8744.5.6.1.1.5.0')  # Serial- returns a string resembling "AA1111A1A1"
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPGet(OIDlist)

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Add the results to a sub-dict in the device dict
            if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[0][1])
            if str(self.SNMPGetInfo[1][1]) != "":
                self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[1][1])
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            if str(self.SNMPGetInfo[2][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[2][1])

        elif '1.3.6.1.4.1.14823.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Aruba'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Aruba device')

            # Get the roles to determine individual members
            OIDlist = '1.3.6.1.4.1.14823.2.2.1.2.1.19.1.2'  # This is the Aruba role OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering role info')
            self.SNMPBulkWalk(OIDlist)

            if len(self.SNMPWalkInfo) > 1:
                # This indicates that there is more than one member present- we need to use the switch list MIB to get both
                # This MIB will not populate for a single unit, so it cannot be used unless there are multiple units

                # Iterate through the entries present
                for RoleRow in range(len(self.SNMPWalkInfo)):
                    # The entries are referenced by their local IP- split this out
                    ModuleRef = str(self.SNMPWalkInfo[RoleRow][0]).split("1.3.6.1.4.1.14823.2.2.1.2.1.19.1.2.", 1)[1]
                    # There isn't a static identifier for a single piece of hardware other than the IP, so we will use the role as the identifier
                    ModuleEntry = str(self.SNMPWalkInfo[RoleRow][1])

                    # Define the OID list
                    OIDlist = ('1.3.6.1.4.1.14823.2.2.1.2.1.19.1.6.' + ModuleRef,  # Host name- returns a string resembling "ABCDEWC01-01-MDF"
                               '1.3.6.1.4.1.14823.2.2.1.2.1.3.0',  # Model- returns a string resembling "A7005-US"- no specific OID for individual members
                               '1.3.6.1.4.1.14823.2.2.1.2.1.19.1.5.' + ModuleRef,  # Controller status- returns an integer- 1 is active and 2 is inactive
                               '1.3.6.1.4.1.14823.2.2.1.2.1.19.1.4.' + ModuleRef,  # Software version- returns a string resembling "6.4.3.7_53990"
                               '1.3.6.1.4.1.14823.2.2.1.2.1.19.1.7.' + ModuleRef)  # Serial number- returns a string resembling "AA1111111"
                    OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                    self.SNMPGet(OIDlist)

                    # Let's instantiate a dict within the device dict for module entry
                    self.DeviceInfo['modules'][ModuleEntry] = {}

                    # Role is an integer response, but is more useful interpreted
                    if ModuleEntry == '1':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'master'
                    elif ModuleEntry == '2':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'standalone'
                    elif ModuleEntry == '3':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'standby'

                    # Add the results to a sub-dict in the device dict
                    if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['hostname'] = str(self.SNMPGetInfo[0][1])
                    if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[1][1])
                    if str(self.SNMPGetInfo[2][1]) != "":
                        # This is an integer response, but is more useful interpreted
                        if str(self.SNMPGetInfo[2][1]) == '1':
                            self.DeviceInfo['modules'][ModuleEntry]['node_status'] = 'up'
                        elif str(self.SNMPGetInfo[2][1]) == '2':
                            self.DeviceInfo['modules'][ModuleEntry]['node_status'] = 'inactive'
                    if str(self.SNMPGetInfo[3][1]) != "":
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[3][1])
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                    if str(self.SNMPGetInfo[4][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[4][1])

            else:
                if str(self.SNMPWalkInfo[0][1]) != "":  # Make sure the line isn't blank

                    # There isn't a static identifier for a single piece of hardware other than the IP, so we will use the role as the identifier
                    ModuleEntry = str(self.SNMPWalkInfo[0][1])

                    # Define the OID list
                    OIDlist = ('1.3.6.1.4.1.14823.2.2.1.2.1.2.0',  # Hostname- returns a string resembling "ABCDEWC01"
                               '1.3.6.1.4.1.14823.2.2.1.2.1.3.0',  # Model- returns a string resembling "A7005-US"
                               '1.3.6.1.4.1.14823.2.2.1.2.1.28.0',  # Version- returns a string resembling "8.2.2.5"
                               '1.3.6.1.4.1.14823.2.2.1.2.1.29.0')  # Serial- returns a string resembling "AA1111111"
                    OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
                    self.SNMPGet(OIDlist)

                    # Let's instantiate a dict within the device dict for module entry
                    self.DeviceInfo['modules'][ModuleEntry] = {}

                    # Role is an integer response, but is more useful interpreted
                    if str(self.SNMPWalkInfo[0][1]) == '1':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'master'
                    elif str(self.SNMPWalkInfo[0][1]) == '2':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'standalone'
                    elif str(self.SNMPWalkInfo[0][1]) == '3':
                        self.DeviceInfo['modules'][ModuleEntry]['controller_role'] = 'standby'

                    # Add the results to a sub-dict in the device dict
                    if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['hostname'] = str(self.SNMPGetInfo[0][1])
                    if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[1][1]).replace("A", "").replace("-US", "")
                    if str(self.SNMPGetInfo[2][1]) != "":
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[2][1])
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                    if str(self.SNMPGetInfo[3][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[3][1])

            # Let's gather the APs
            OIDlist = '1.3.6.1.4.1.14823.2.2.1.5.2.1.4.1.13'  # This is the Aruba AP model OID
            OutputLog.debug(f'DID {self.BAPDID}- Gathering AP info')
            self.SNMPBulkWalk(OIDlist)

            # We need to loop through the models given to gather further information
            for ModelRow in range(len(self.SNMPWalkInfo)):  # Start at line 1 as this is zero-indexed
                Model = self.SNMPWalkInfo[ModelRow][1]
                if str(Model) != "":  # Make sure the line isn't blank
                    try:
                        APModels  # See if the var exists first
                    except NameError:
                        APModels = []  # If not, define it

                    APModels.extend([str(Model)])  # Add the AP to the list

            try:  # If the AP model list is present, let's aggregate it
                ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                    self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
            except NameError:
                pass

        elif '1.3.6.1.4.1.14988.' in self.DeviceInfo['vendor']:
            self.DeviceInfo['vendor'] = 'Mikrotik'
            OutputLog.debug(f'DID {self.BAPDID}- Processing Mikrotik device')

            # Define the OID list
            OIDlist = ('1.3.6.1.2.1.1.1.0',
                       # System description- returns a string resembling "RouterOS RB1200"- will show the model
                       '1.3.6.1.4.1.14988.1.1.7.3.0',  # Serial- returns a string resembling "11A111A1A1AA"
                       '1.3.6.1.4.1.14988.1.1.7.4.0',  # Board version- returns a string resembling "6.43.2"
                       '1.3.6.1.4.1.14988.1.1.4.4.0')  # Software license version- returns a string resembling "6.43.2"- this is also the current package versions
            OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
            self.SNMPGet(OIDlist)

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Add the results to a sub-dict in the device dict
            if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[0][1]).split().pop()
            if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[1][1])
            if str(self.SNMPGetInfo[2][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['firmware'] = str(self.SNMPGetInfo[2][1])
            if str(self.SNMPGetInfo[3][1]) != "":
                self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[3][1])
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']

        elif '1.3.6.1.4.1.25053.' in self.DeviceInfo['vendor']:
            OutputLog.debug(f'DID {self.BAPDID}- Processing Ruckus device')

            # Ruckus model lines are defined in the enterprise OID- 5 is a ZD, and 10/11 are SmartZones
            if '1.3.6.1.4.1.25053.3.1.5' in self.DeviceInfo['vendor']:
                OutputLog.debug(f'DID {self.BAPDID}- Processing Ruckus ZoneDirector')
                # Define the OID list
                OIDlist = ('1.3.6.1.4.1.25053.1.2.1.1.1.1.9.0',  # Model- returns a string resembling "ZD1106"
                           '1.3.6.1.4.1.25053.1.2.1.1.1.1.12.0',  # Licensed APs- returns a Gauge32 resembling "50"
                           '1.3.6.1.4.1.25053.1.2.1.1.1.1.15.0',  # Serial- returns a string resembling "111111111111"
                           '1.3.6.1.4.1.25053.1.2.1.1.1.1.18.0',
                           # Version- returns a string resembling "9.7.2.0 build 20"
                           '1.3.6.1.4.1.25053.1.2.1.1.1.1.30.0',
                           # Redundancy configuration- returns an integer- 1 is master, 2 is standby and 3 is noredundancy
                           '1.3.6.1.4.1.25053.1.2.1.1.1.1.31.0')  # Redundancy state- returns an integer- 1 is connected and 2 is disconnected

                # Get the OIDs as specified above
                OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                self.SNMPGet(OIDlist)

                ModuleEntry = str('1')  # This is used for variable references- as there are no stacks there will only be one entry

                # Let's instantiate a dict within the device dict for module entry
                self.DeviceInfo['modules'][ModuleEntry] = {}

                # Add the results to a sub-dict in the device dict
                if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['model'] = str(self.SNMPGetInfo[0][1]).upper()
                if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['licensed_aps'] = str(self.SNMPGetInfo[1][1])
                if str(self.SNMPGetInfo[2][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[2][1])
                if str(self.SNMPGetInfo[3][1]) != "":
                    self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[3][1])
                    self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                if str(self.SNMPGetInfo[4][1]) != "":
                    # This is an integer response, but is more useful interpreted
                    if str(self.SNMPGetInfo[4][1]) == '1':
                        self.DeviceInfo['modules'][ModuleEntry]['redundancy_config'] = 'master'
                    elif str(self.SNMPGetInfo[4][1]) == '2':
                        self.DeviceInfo['modules'][ModuleEntry]['redundancy_config'] = 'standby'
                    elif str(self.SNMPGetInfo[4][1]) == '3':
                        self.DeviceInfo['modules'][ModuleEntry]['redundancy_config'] = 'noredundancy'

                if str(self.SNMPGetInfo[5][1]) != "":
                    # This is an integer response, but is more useful interpreted
                    if str(self.SNMPGetInfo[5][1]) == '1':
                        self.DeviceInfo['modules'][ModuleEntry]['peer_state'] = 'connected'
                    elif str(self.SNMPGetInfo[5][1]) == '2':
                        self.DeviceInfo['modules'][ModuleEntry]['peer_state'] = 'disconnected'

                # Let's gather the APs- we need to get the models and firmware versions
                OIDlist = '1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.4'  # This is the ZD AP model OID
                OutputLog.debug(f'DID {self.BAPDID}- Gathering AP models')
                self.SNMPBulkWalk(OIDlist)
                ModelList = self.SNMPWalkInfo
                OIDlist = '1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.7'  # This is the ZD AP model OID
                OutputLog.debug(f'DID {self.BAPDID}- Gathering AP firmware versions')
                self.SNMPBulkWalk(OIDlist)
                VersionList = self.SNMPWalkInfo

                # We need to loop through the models given to gather further information
                for ModelRow in range(len(ModelList)):
                    Model = ModelList[ModelRow][1]
                    Version = VersionList[ModelRow][1]
                    if str(Model) != "":  # Make sure the line isn't blank
                        try:
                            APModels  # See if the var exists first
                        except NameError:
                            APModels = []  # If not, define it

                        if self.DeviceInfo['modules'][ModuleEntry]['version'].replace(" build ", ".") in str(Version):  # Make sure the AP's version matches the ZD's version to make sure the AP is actually joined, or has been recently
                            APModels.extend([str(Model).upper()])  # Add the AP to the list

                try:  # If the AP model list is present, let's aggregate it
                    ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                    for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                        self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
                except NameError:
                    pass

            elif re.search('1.3.6.1.4.1.25053.3.1.[10]{2}', str(self.DeviceInfo['vendor'])) is not None:
                OutputLog.debug(f'DID {self.BAPDID}- Processing Ruckus SmartZone')

                # Need to walk the model, as the cluster is going to report as multiples
                OIDlist = '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.3'  # This is the SZ model OID
                OutputLog.debug(f'DID {self.BAPDID}- Gathering model info')
                self.SNMPBulkWalk(OIDlist)

                # We need to loop through the models given to gather further information
                for ModelRow in range(len(self.SNMPWalkInfo)):
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Make sure the line isn't blank

                        # Set the entry reference to use for further information
                        # Split on the OID walked
                        ModuleRef = str(self.SNMPWalkInfo[ModelRow][0]).split("1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.3.", 1)[1]
                        ModuleEntry = str('' + ModuleRef.split('.').pop())  # This is used for variable references- shortened because the full reference is quite long

                        # Define the OID list
                        OIDlist = ('1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.1.' + ModuleRef,
                                   # Serial- returns a string resembling "111111111111"
                                   '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.2.' + ModuleRef,
                                   # Node name- returns a string resembling "ABCDEWC01-01-MDF"
                                   '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.9.' + ModuleRef,
                                   # Software version- returns a string resembling "3.6.2.0.222"
                                   '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.10.' + ModuleRef,
                                   # Licensed APs- returns a Gauge32 resembling "200"
                                   '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.17.' + ModuleRef,
                                   # Node status- returns an integer- 0 is out-of-service and 8 is in-service
                                   '1.3.6.1.4.1.25053.1.8.1.1.1.1.1.1.18.' + ModuleRef)  # Cluster status- returns an integer- 0 is in-service, 1 is out-of-service, 2 is maintenance and 4 is network-partition-suspected
                        OutputLog.debug(f'DID {self.BAPDID}- Gathering further info')
                        self.SNMPGet(OIDlist)

                        # Let's instantiate a dict within the device dict for module entry
                        self.DeviceInfo['modules'][ModuleEntry] = {}

                        # Add the results to a sub-dict in the device dict
                        self.DeviceInfo['modules'][ModuleEntry]['model'] = str(Model)
                        if str(self.SNMPGetInfo[0][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['serial'] = str(self.SNMPGetInfo[0][1])
                        if str(self.SNMPGetInfo[1][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['node_name'] = str(self.SNMPGetInfo[1][1])
                        if str(self.SNMPGetInfo[2][1]) != "":
                            self.DeviceInfo['modules'][ModuleEntry]['version'] = str(self.SNMPGetInfo[2][1])
                            self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                        if str(self.SNMPGetInfo[3][1]) != "": self.DeviceInfo['modules'][ModuleEntry]['licensed_aps'] = str(self.SNMPGetInfo[3][1])
                        if str(self.SNMPGetInfo[4][1]) != "":
                            # This is an integer response, but is more useful interpreted
                            if str(self.SNMPGetInfo[4][1]) == '0':
                                self.DeviceInfo['modules'][ModuleEntry]['node_status'] = 'out-of-service'
                            elif str(self.SNMPGetInfo[4][1]) == '8':
                                self.DeviceInfo['modules'][ModuleEntry]['node_status'] = 'in-service'

                        if str(self.SNMPGetInfo[5][1]) != "":
                            # This is an integer response, but is more useful interpreted
                            if str(self.SNMPGetInfo[5][1]) == '0':
                                self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'in-service'
                            elif str(self.SNMPGetInfo[5][1]) == '1':
                                self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'out-of-service'
                            elif str(self.SNMPGetInfo[5][1]) == '2':
                                self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'maintenance'
                            elif str(self.SNMPGetInfo[5][1]) == '4':
                                self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'network-partition-suspected'

                    # Let's gather the APs
                OIDlist = '1.3.6.1.4.1.25053.1.4.2.1.1.2.2.1.8'  # This is the SZ AP model OID
                OutputLog.debug(f'DID {self.BAPDID}- Gathering AP info')
                self.SNMPBulkWalk(OIDlist)

                # We need to loop through the models given to gather further information
                for ModelRow in range(len(self.SNMPWalkInfo)):
                    Model = self.SNMPWalkInfo[ModelRow][1]
                    if str(Model) != "":  # Make sure the line isn't blank
                        try:
                            APModels  # See if the var exists first
                        except NameError:
                            APModels = []  # If not, define it

                        APModels.extend([str(Model)])  # Add the AP to the list

                try:  # If the AP model list is present, let's aggregate it
                    ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                    for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                        self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
                except NameError:
                    pass

            # Defined here as this was used prior
            self.DeviceInfo['vendor'] = 'Ruckus'

        else:
            OutputLog.error(f'DID {self.BAPDID}- {self.DeviceInfo["vendor"]} is not currently defined!')
            self.DeviceInfo['snmp_reply'] = 'Not_Supported'
            return

    def CLIGather(self):

        # There is a possibility there is no SSH port listed- check for this
        if 'adminport' not in self.DeviceInfo:
            OutputLog.error(f'DID {self.BAPDID}- Proceeded to CLI gathering, but no admin port defined- please check the device to ensure a SSH port is present!')
            self.DeviceInfo['ssh_reply'] = 'None'
            return

        # See if Netmiko can determine the applicable session profile to use
        ProfileResults = SSHProfile(self.DeviceInfo)

        # Make sure we got a result, then update the credentials listed if necessary for later tries
        if ProfileResults:
            match_connection = ProfileResults['device_type']
            self.DeviceInfo['username'] = ProfileResults['username']
            self.DeviceInfo['password'] = ProfileResults['password']
            self.DeviceInfo['modules'] = {}
        else:
            self.DeviceInfo['ssh_reply'] = 'None'
            return

        self.DeviceInfo['ssh_reply'] = 'Complete'

        # Start the connection to the device again with the new information
        device_connection, postloginbanner = SSHSetup(self.DeviceInfo, match_connection)
        if device_connection:
            pass
        else:
            self.DeviceInfo['ssh_reply'] = 'None'
            return

        # We will try getting the prompt, but depending on the device this may not complete based on the connection type and escape characters
        try:
            DevicePrompt = device_connection.find_prompt()
        except (NetMikoTimeoutException, OSError, ValueError):  # Catch SSH connection errors
            OutputLog.debug(f'{self.BAPDID} - Unable to gather the prompt- continuing for now')
            DevicePrompt = ""

        # Time to figure out a few things. Let's split these off based on what was determined before
        if match_connection == 'cisco_asa':
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from ASA device')
            self.DeviceInfo['vendor'] = 'Cisco'
            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Gather additional information
            showinventory = device_connection.send_command("show inventory")
            showversion = device_connection.send_command("show version")
            showfailover = device_connection.send_command("show failover")
            self.DeviceInfo['hostname'] = device_connection.send_command("show hostname").strip()

            # Parse out the relevant information
            Version = re.search('(?<=Appliance Software Version ).*', showversion)
            if Version is not None:
                self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showversion}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Model = re.search('(?<=PID: ).*(?=, VID:)', showinventory)
            if Model is not None:
                self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showinventory}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Serial = re.search('(?<=SN: ).*', showinventory)
            if Serial is not None:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showinventory}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            HardwareRev = re.search('(?<=VID: ).*(?=, SN:)', showinventory)
            if HardwareRev is not None:
                self.DeviceInfo['modules'][ModuleEntry]['hardwarerev'] = HardwareRev.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showinventory}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            # Check to see if failover is enabled, and if so create an entry for the second unit
            if "Failover On" in str(showfailover):
                self.DeviceInfo['failover'] = True

                # Create an entry for the second unit
                self.DeviceInfo['2'] = {}

                # Find the name of the interface being used for management access
                for line in showfailover.split("\n"):
                    if re.search(self.DeviceInfo['ipaddress'], line):
                        ManagementInterface = re.search('(?<=Interface ).*', line).group().split(" ")[0]
                    if "Version:" in line:
                        self.DeviceInfo['2']['version'] = re.search('(?<=Mate ).*', line).group()
                    if "Serial Number:" in line:
                        self.DeviceInfo['2']['serial'] = re.search('(?<=Mate ).*', line).group()

                # Find the IP of the secondary unit's management interface
                for line in showfailover.split("\n"):
                    if re.search(ManagementInterface, line):
                        if re.search(self.DeviceInfo['ipaddress'], line):
                            pass
                        else:
                            self.DeviceInfo['peeripaddress'] = re.search('(?<= \().*(?=\):)', line).group()

            else:
                self.DeviceInfo['failover'] = False

        elif match_connection == 'cisco_ios':
            self.DeviceInfo['vendor'] = 'Cisco'
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from IOS device')

            # See if the switch is stacked
            showversion = device_connection.send_command("show version")
            showinventory = device_connection.send_command("show inventory")
            self.DeviceInfo['hostname'] = re.search('.*(?= uptime is)', showversion).group().strip()

            PreviousInventoryLine = ""  # We may need to reference the previous line for some items

            for InventoryLine in showinventory.splitlines():
                if 'PID:' not in InventoryLine:
                    PreviousInventoryLine = InventoryLine
                    continue  # Don't process lines that are simply a text description

                if re.search('C[0-9]{4}-SPE|CISCO[0-9]{4}', str(InventoryLine)) is not None:
                    if re.search('CISCO[0-9]{4}', str(InventoryLine)) is not None:  # This is the actual router model
                        ModuleEntry = str('1')
                    else:  # This is the SPE if present
                        ModuleEntry = str('2')

                    # Let's instantiate a dict within the device dict for module entry
                    self.DeviceInfo['modules'][ModuleEntry] = {}

                    # Parse out the relevant information
                    Version = re.search('(?<=, Version ).*(?=, )', showversion)
                    if Version is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showversion}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Model = re.search('(?<=PID: ).*(?=, VID:)', InventoryLine)
                    if Model is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Serial = re.search('(?<=SN: ).*', InventoryLine)
                    if Serial is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    HardwareRev = re.search('(?<=VID: ).*(?=, SN:)', InventoryLine)
                    if HardwareRev is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['hardwarerev'] = HardwareRev.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                elif re.search('WS-C[0-9]{4}|WS-.*SUP', str(InventoryLine)) is not None:
                    if 'Supervisor' in showinventory:
                        if re.search('WS-.*SUP', str(InventoryLine)) is not None:
                            # We will need the previous line to determine the number for the module entry
                            ModuleNumber = re.search('(?<=slot )[0-9]', PreviousInventoryLine)

                            if ModuleNumber is not None:
                                ModuleNumber = ModuleNumber.group()
                            else:
                                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {PreviousInventoryLine}')
                                self.DeviceInfo['ssh_reply'] = 'Incomplete'

                            ModuleEntry = str('' + ModuleNumber)

                            # Get the version on the module
                            moduleversion = device_connection.send_command(f"show version switch {ModuleNumber} running")

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                            # Parse out the relevant information
                            VersionLine = re.search('(?<=Package: Base, version: ).*(?=,)', moduleversion)  # This displays the individual module's versions in case of a mismatch
                            if VersionLine is not None:
                                self.DeviceInfo['modules'][ModuleEntry]['version'] = VersionLine.group()
                                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                            else:
                                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {moduleversion}')
                                self.DeviceInfo['ssh_reply'] = 'Incomplete'
                        else:
                            # This will be the chassis model
                            ModuleEntry = str('chassis')

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {}

                    else:
                        if re.search('c3[68]xx Stack', str(PreviousInventoryLine)) is not None:
                            continue  # This is a fake model for stack representation that mirrors the master

                        # We will need the previous line to determine the switch number for the module entry
                        SwitchNumber = re.search('(?<=NAME: ").*(?=", DESCR:)', PreviousInventoryLine)

                        if SwitchNumber is not None:
                            SwitchNumber = SwitchNumber.group().strip().replace("Switch ", "")
                        else:
                            OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                            OutputLog.debug(f'{self.BAPDID} - {PreviousInventoryLine}')
                            self.DeviceInfo['ssh_reply'] = 'Incomplete'

                        ModuleEntry = str('' + SwitchNumber)

                        # Let's instantiate a dict within the device dict for module entry
                        self.DeviceInfo['modules'][ModuleEntry] = {}

                        # Parse out the relevant information
                        VersionLine = re.search('(?<=    '+SwitchNumber+' ).*', showversion)  # This grabs the individual line from show version as this displays the individual switch's versions in case of a mismatch
                        if VersionLine is not None:
                            self.DeviceInfo['modules'][ModuleEntry]['version'] = VersionLine.group().split()[2]
                            self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                        else:
                            OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                            OutputLog.debug(f'{self.BAPDID} - {showversion}')
                            self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Model = re.search('(?<=PID: ).*(?=, VID:)', InventoryLine)
                    if Model is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Serial = re.search('(?<=SN: ).*', InventoryLine)
                    if Serial is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    HardwareRev = re.search('(?<=VID: ).*(?=, SN:)', InventoryLine)
                    if HardwareRev is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['hardwarerev'] = HardwareRev.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {InventoryLine}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                PreviousInventoryLine = InventoryLine

        elif match_connection == 'linux':
            self.DeviceInfo['vendor'] = 'Linux'
            pass
        elif 'ruckus#' in DevicePrompt:
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from a ZoneDirector')
            self.DeviceInfo['vendor'] = 'Ruckus'

            showsysinfo = device_connection.send_command("show sysinfo")
            showconfig = device_connection.send_command_timing("show config")

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            Hostname = re.search('(?<=Name= ).*', showsysinfo)
            if Hostname is not None:
                self.DeviceInfo['hostname'] = Hostname.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Model = re.search('(?<=Model= ).*', showsysinfo)
            if Model is not None:
                self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Version = re.search('(?<=Version= ).*', showsysinfo)
            if Version is not None:
                self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Serial = re.search('(?<=Serial Number= ).*', showsysinfo)
            if Serial is not None:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            LicensedAPs = re.search('(?<=Licensed APs= ).*', showsysinfo)
            if LicensedAPs is not None:
                self.DeviceInfo['modules'][ModuleEntry]['licensed_aps'] = LicensedAPs.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            LocalSRStatus = re.search('(?<=Local Connect Status= ).*', showconfig)
            if LocalSRStatus is not None:
                self.DeviceInfo['modules'][ModuleEntry]['redundancy_config'] = LocalSRStatus.group().strip()
            else:
                self.DeviceInfo['modules'][ModuleEntry]['redundancy_config'] = 'noredundancy'

            PeerSRStatus = re.search('(?<=Peer Connect Status = ).*', showconfig)
            if PeerSRStatus is not None:
                self.DeviceInfo['modules'][ModuleEntry]['peer_state'] = PeerSRStatus.group().strip()
            else:
                self.DeviceInfo['modules'][ModuleEntry]['peer_state'] = 'disconnected'

            # Make a blank list for the APs
            APModels = []

            # Split out the sections for each AP
            for APSection in showconfig.split('MAC Address'):
                if 'Model' in APSection:
                    Model = re.search('(?<=Model= ).*', APSection).group().replace(" ", "")
                    APName = re.search('(?<=Device Name= ).*', APSection).group().replace(" ", "")
                    if APName is not "":
                        APModels.append(Model)

            try:  # If the AP model list has entries, let's aggregate them
                ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                    self.DeviceInfo.setdefault('apmodels', []).append([apmodel.upper(), ModelCounts[apmodel]])
            except NameError:
                pass

        elif bool(re.search('MSM[0-9]{3}', postloginbanner)):
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from MSM device')
            self.DeviceInfo['vendor'] = 'HP'

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Enable the hard way
            device_connection.send_command("enable", expect_string=r'CLI#')

            # Gather additional information
            showsystem = device_connection.send_command("show system info")

            # Parse out the relevant information
            self.DeviceInfo['hostname'] = "HOSTNAME"
            Version = re.search('(?<=Firmware Version: ).*(?= Load 1min:)', showsystem)
            if Version is not None:
                self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsystem}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Model = re.search('MSM[0-9]{3}', postloginbanner)
            if Model is not None:
                self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {postloginbanner}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Serial = re.search('(?<=Serial Number:).*(?= CPU use now:)', showsystem)
            if Serial is not None:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsystem}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

        elif "Display NSE setup" in postloginbanner:
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from Nomadix device')
            self.DeviceInfo['vendor'] = 'Nomadix'

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Gather additional information- we need to start the summary to display the NSE ID, then send 'esc' to quit
            device_connection.send_command("configuration", expect_string=r'Configuration>')
            NSEID = device_connection.send_command("summary", expect_string=r'to abort --')
            device_connection.write_channel('\x1b')

            device_connection.send_command("b", expect_string=DevicePrompt)  # Back to the base menu

            SystemMenu = device_connection.send_command("system", expect_string=r'System>')
            if "failover" in SystemMenu:  # If this is present, the feature is licensed so we need to check the status
                device_connection.write_channel("failover\n")
                time.sleep(1)
                FailoverState = device_connection.read_channel()  # Must use write/read channel due to escape sequences
                if "enabled" in FailoverState:
                    self.DeviceInfo['failover'] = True  # There is no method to determine the status of a peer, so we must assume it is present
                else:
                    self.DeviceInfo['failover'] = False  # There is no method to determine the status of a peer, so we must assume it is present

                device_connection.write_channel('\x1b')  # Back out of the configuration menu
            else:
                self.DeviceInfo['failover'] = False  # The feature is not licensed

            # Parse out the relevant information
            self.DeviceInfo['hostname'] = ''

            Version = re.search('(?<=[AEX]G [0-9]{4} ).*(?=:)', postloginbanner)
            if Version is not None:
                self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather version information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {postloginbanner}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Model = re.search('[AEX]G [0-9]{4}.*(?= v)', postloginbanner)
            if Model is not None:
                self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().replace(" ", "")
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather model information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {postloginbanner}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Serial = re.search('(?<=NSE ID ).*', NSEID)
            if Serial is not None:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {NSEID}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

        elif 'MikroTik RouterOS' in postloginbanner:
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from Mikrotik device')
            # This is likely a Mikrotik device based on the banner present.  Disconnect the current session
            device_connection.disconnect()
            # Redefine the connection info based on this
            self.DeviceInfo['vendor'] = 'Mikrotik'
            match_connection = 'mikrotik_routeros'

            # Reconnect to the device
            device_connection, postloginbanner = SSHSetup(self.DeviceInfo, match_connection)
            time.sleep(1)
            if device_connection:
                pass
            else:
                self.DeviceInfo['ssh_reply'] = 'None'
                return

            ModuleEntry = str('1')  # This is used for variable references- as there are no stacks or APs there will only be one entry

            # Let's instantiate a dict within the device dict for module entry
            self.DeviceInfo['modules'][ModuleEntry] = {}

            # Gather additional information
            systemidentity = device_connection.send_command("system identity print")
            systempackage = device_connection.send_command("system package print")
            systemrouterboard = device_connection.send_command("system routerboard print")

            # Parse out the relevant information
            Hostname = re.search('(?<=  name: ).*', systemidentity)
            if Hostname is not None:
                self.DeviceInfo['hostname'] = Hostname.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {systemidentity}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            PackageVersion = re.search('(?<=system ) *[0-9].[0-9]{1,2}[.0-9]{0,3}', systempackage)
            if PackageVersion is not None:
                self.DeviceInfo['modules'][ModuleEntry]['version'] = PackageVersion.group().replace(" ", "")
                self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {systempackage}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Model = re.search('(?<=model: ).*', systemrouterboard)
            if Model is not None:
                self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {systemrouterboard}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            Serial = re.search('(?<=serial-number: ).*', systemrouterboard)
            if Serial is not None:
                self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {systemrouterboard}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            FirmwareVersion = re.search('(?<=current-firmware: ).*', systemrouterboard)
            if FirmwareVersion is not None:
                self.DeviceInfo['modules'][ModuleEntry]['firmware'] = FirmwareVersion.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {systemrouterboard}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

        elif "WatchGuard Fireware OS" in postloginbanner:
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from Watchguard device')
            self.DeviceInfo['vendor'] = 'Watchguard'

            # Gather additional information- we need to start the summary to display the NSE ID, then send 'esc' to quit
            showsysinfo = device_connection.send_command("show sysinfo")
            showcluster = device_connection.send_command("show cluster")
            
            # Parse out the relevant information
            Hostname = re.search('(?<=system name)\s+: (.*)', showsysinfo)
            if Hostname is not None:
                self.DeviceInfo['hostname'] = Hostname.group(1).strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather hostname- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'

            if 'Member' in showcluster:  # If any members show up clustering is enabled
                showclusterstatus = device_connection.send_command("show cluster status")  # This command doesn't work unless clustering is enabled

                for ClusterMember in range(len(re.findall('Member Name', showcluster))):
                    ModuleEntry = str(ClusterMember + 1)

                    # Let's instantiate a dict within the device dict for module entry
                    self.DeviceInfo['modules'][ModuleEntry] = {}

                    Model = re.search('(?<=: ).*', re.findall('device_model.*', showclusterstatus)[ClusterMember])
                    if Model is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showclusterstatus}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Version = re.search('(?<=version)\s+: (.*)', showsysinfo)
                    if Version is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group(1).strip()
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather version- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    Serial = re.search('(?<=: ).*', re.findall('member_id.*', showclusterstatus)[ClusterMember])
                    if Serial is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showclusterstatus}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    ClusterStatus = re.search('(?<=: ).*', re.findall('role.*', showclusterstatus)[ClusterMember])
                    if ClusterStatus is not None:
                        self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = ClusterStatus.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather cluster status- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showclusterstatus}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

            else:
                ModuleEntry = str('1')  # This is used for variable references- the first is always present

                # Let's instantiate a dict within the device dict for module entry
                self.DeviceInfo['modules'][ModuleEntry] = {}

                Model = re.search('(?<=system model)\s+: (.*)', showsysinfo)
                if Model is not None:
                    self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group(1).strip()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather model- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                Version = re.search('(?<=version)\s+: (.*)', showsysinfo)
                if Version is not None:
                    self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group(1).strip()
                    self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather version- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                Serial = re.search('(?<=serial number)\s+: (.*)', showsysinfo)
                if Serial is not None:
                    self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group(1).strip()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showsysinfo}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                self.DeviceInfo['modules'][ModuleEntry]['cluster_status'] = 'disabled'

        elif bool(re.search('[0-9]{2};[0-9]{1,2}H', postloginbanner)):
            OutputLog.info(f'{self.BAPDID} - Getting CLI information from Procurve device')
            # This is likely a Procurve device based on the escape characters present.  Disconnect the current session
            device_connection.disconnect()
            # Redefine the connection info based on this
            self.DeviceInfo['vendor'] = 'Hewlett-Packard'
            match_connection = 'hp_procurve'

            # Reconnect to the device and grab the prompt again
            device_connection, postloginbanner = SSHSetup(self.DeviceInfo, match_connection)
            if device_connection:
                pass
            else:
                self.DeviceInfo['ssh_reply'] = 'None'
                return
            # Send a carriage return to make sure we're out of the starting page
            device_connection.send_command("\n")

            # See if the switch is stacked
            showsystem = device_connection.send_command("show system")
            self.DeviceInfo['hostname'] = re.search('(?<=System Name        : ).*', showsystem).group().strip()

            if "Member :" in showsystem:  # This indicates the switch is in a stack
                # Gather further information
                if "VSF-" in showsystem:
                    showstack = device_connection.send_command("show vsf detail")
                else:
                    showstack = device_connection.send_command("show stacking detail")
                softwareversion = re.search('(?<=Software Version : )[A-Z]{1,2}.[0-9]{2}.[0-9]{2}.*', showstack).group()

                for Member in showstack.split("\n\n\nM"):

                    if " Topology" in Member:
                        pass

                    else:
                        # Set the entry reference to use for further information
                        # Split on "Member ID"
                        ModuleRef = str(re.search('(?<=ember ID        : ).*', Member).group().strip())
                        ModuleEntry = str('' + ModuleRef)  # This is used for variable references

                        # Let's instantiate a dict within the device dict for module entry
                        self.DeviceInfo['modules'][ModuleEntry] = {}

                        # Add the results to a sub-dict in the device dict
                        self.DeviceInfo['modules'][ModuleEntry]['version'] = str(softwareversion)
                        self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                        ModelLine = re.search('(?<= HP | Aru).*(?= Switch)', Member)
                        if ModelLine is not None:
                            Model = re.search(' [E]?[0-9]{4}[0-9A-Za-z-]*', ModelLine.group())
                            self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().replace(" ", "")
                        else:
                            OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                            OutputLog.debug(f'{self.BAPDID} - {Member}')
                            self.DeviceInfo['ssh_reply'] = 'Incomplete'

                        Serial = re.search('(?<=Serial Number    : ).*', Member)
                        if Serial is not None:
                            self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
                        else:
                            OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                            OutputLog.debug(f'{self.BAPDID} - {Member}')
                            self.DeviceInfo['ssh_reply'] = 'Incomplete'

            else:
                # Gather further information
                showversion = device_connection.send_command("show version")
                showlldp = device_connection.send_command("show lldp info local-device")

                # Let's instantiate a dict within the device dict for module entry
                ModuleEntry = str('1')  # This is used for variable references- as this is not a stack there will only be one entry
                self.DeviceInfo['modules'][ModuleEntry] = {}

                # Parse out the relevant information
                Version = re.search('[A-Z]{1,2}.[0-9]{2}.[0-9]{2}.*', showversion)
                if Version is not None:
                    self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group()
                    self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showversion}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                ModelLine = re.search('(?<=System Description : ).*(?=, revision)', showlldp)
                if ModelLine is not None:
                    Model = re.search(' [E]?[0-9]{4}[0-9A-Za-z-]*', ModelLine.group())
                    self.DeviceInfo['modules'][ModuleEntry]['model'] = Model.group().replace(" ", "")
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showlldp}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                Serial = re.search('(?<=Serial Number      : ).*', showsystem)
                if Serial is not None:
                    self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showsystem}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

        else:
            # Not caught in anything prior- let's try a few standard commands to see if we can determine the device type
            try:
                seekversion = device_connection.send_command_timing('show version')
            except OSError:
                OutputLog.error(f'{self.BAPDID} - Unable to properly seek version because the connection in closed- see debug logs!')
                self.DeviceInfo['ssh_reply'] = 'Incomplete'
                return

            if 'Aruba Operating System Software.' in seekversion:
                # Disconnect the current session
                device_connection.disconnect()
                # Redefine the connection info based on this
                self.DeviceInfo['vendor'] = 'Aruba'
                match_connection = 'aruba_os'

                # Reconnect to the device
                device_connection, postloginbanner = SSHSetup(self.DeviceInfo, match_connection)
                if device_connection:
                    pass
                else:
                    self.DeviceInfo['ssh_reply'] = 'None'
                    return
                # Send a carriage return to make sure we're out of the starting page
                device_connection.send_command("\n")

                # This is an ArubaOS switch or WLC
                if bool(re.search('Aruba[0-9]{3,4}', seekversion)):
                    # This is a WLC
                    showswitches = device_connection.send_command('show switches')
                    showroleinfo = device_connection.send_command('show roleinfo')  # This is not explicitly necessary, but is used to determine the 'local' device
                    showswitchinfo = device_connection.send_command('show switchinfo')
                    showserial = device_connection.send_command('show inventory | include "System Serial"')
                    aplist = device_connection.send_command('show ap database status up | include "  Up "')

                    # Parse out the relevant information
                    Hostname = re.search('(?<=Hostname is ).*', showswitchinfo)
                    if Hostname is not None:
                        self.DeviceInfo['hostname'] = Hostname.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather hostname- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showswitchinfo}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    LocalRole = re.search('(?<=switchrole:).*', showroleinfo)
                    if LocalRole is not None:
                        LocalRole = LocalRole.group().strip()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather local role- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {showroleinfo}')
                        self.DeviceInfo['ssh_reply'] = 'Incomplete'

                    # We need to parse the rows from switches to determine module entries
                    for SwitchRow in showswitches.splitlines():
                        if bool(re.search('Aruba[0-9]{3,4}', SwitchRow)):  # Skip the non-relevant lines

                            SwitchRowSplit = list(filter(None, SwitchRow.split('  ')))  # We will split this based on columns, present as double spaces (removing blank entries)
                            if 'IPv6 Address' in showswitches:  # This is the only change present between major versions and not necessary
                                del SwitchRowSplit[1]

                            # Role is an integer response, but is more useful interpreted. MD and Standalone are identical for the case presented here
                            if 'master' in SwitchRowSplit[3]:
                                ModuleEntry = '1'
                                ControllerRole = 'master'
                            elif 'standalone' in SwitchRowSplit[3]:
                                ModuleEntry = '2'
                                ControllerRole = 'standalone'
                            elif 'standby' in SwitchRowSplit[3]:
                                ModuleEntry = '3'
                                ControllerRole = 'standby'
                            elif 'MD' in SwitchRowSplit[3]:
                                ModuleEntry = '2'
                                ControllerRole = 'MD'
                            else:
                                ModuleEntry = '5'
                                ControllerRole = 'Unknown'

                            # Let's instantiate a dict within the device dict for module entry
                            self.DeviceInfo['modules'][ModuleEntry] = {'controller_role': ControllerRole}

                            if ControllerRole == LocalRole:  # If the controller role matches the one we are logged into, then the serial can be applied to this entry
                                Serial = re.search('(?<=: )[0-9A-Z]* ', showserial)
                                if Serial is not None:
                                    self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().replace(" ", "")
                                else:
                                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial- see debug logs!')
                                    OutputLog.debug(f'{self.BAPDID} - {showserial}')
                                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                            self.DeviceInfo['modules'][ModuleEntry]['hostname'] = SwitchRowSplit[1].strip()
                            self.DeviceInfo['modules'][ModuleEntry]['model'] = SwitchRowSplit[4].strip().replace("Aruba", "")
                            self.DeviceInfo['modules'][ModuleEntry]['version'] = SwitchRowSplit[5].strip()
                            self.DeviceInfo['summaryversion'] = self.DeviceInfo['modules'][ModuleEntry]['version']
                            self.DeviceInfo['modules'][ModuleEntry]['node_status'] = SwitchRowSplit[6].strip()

                    # Parse the APs into an aggregate, if present
                    for ModelRow in aplist.splitlines():
                        ModelRowSplit = list(filter(None, ModelRow.split('  ')))  # We will split this based on columns, present as double spaces (removing blank entries)
                        Model = ModelRowSplit[2].strip()
                        if str(Model) != "":  # Make sure the line isn't blank
                            try:
                                APModels  # See if the var exists first
                            except NameError:
                                APModels = []  # If not, define it

                            APModels.extend([str(Model)])  # Add the AP to the list

                    try:  # If the AP model list is present, let's aggregate it
                        ModelCounts = Counter(APModels)  # Aggregate the APs into counts
                        for apmodel in ModelCounts:  # Counter will output a dict, so let's take the entries and add to the device dict
                            self.DeviceInfo.setdefault('apmodels', []).append([apmodel, ModelCounts[apmodel]])
                    except NameError:
                        pass

            elif bool(re.search('Ruckus Networks', seekversion)) and ('UNIT 1: compiled' in seekversion):
                # Disconnect the current session
                device_connection.disconnect()
                # Redefine the connection info based on this
                self.DeviceInfo['vendor'] = 'Brocade/Ruckus'
                match_connection = 'ruckus_fastiron'

                # Reconnect to the device
                device_connection, postloginbanner = SSHSetup(self.DeviceInfo, match_connection)
                if device_connection:
                    pass
                else:
                    self.DeviceInfo['ssh_reply'] = 'None'
                    return

                # Make sure the paging display is disable
                device_connection.disable_paging()

                # Gather more information
                DevicePrompt = device_connection.find_prompt()
                showstack = device_connection.send_command('show stack')
                showversion = device_connection.send_command('show version')

                # Parse out the relevant information
                Hostname = re.search('(?<=@).*(?=#)', DevicePrompt)
                if Hostname is not None:
                    self.DeviceInfo['hostname'] = Hostname.group().strip()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {DevicePrompt}')
                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                for SwitchRow in showstack.splitlines():  # Iterate through the list of switches
                    if 'ICX' in SwitchRow:  # Each relevant line will display as "1  S ICX7150-48PF  active  aaaa.bbbb.cccc 255 local   Ready"
                        SwitchRowSplit = list(filter(None, SwitchRow.split()))
                        ModuleEntry = SwitchRowSplit[0]  # The first number is the switch number
                        SwitchModel = SwitchRowSplit[2]

                        # Let's instantiate a dict within the device dict for module entry
                        self.DeviceInfo['modules'][ModuleEntry] = {'model': SwitchModel}

                        # Loop through the lines in "show version" to determine the serial numbers and versions- the line X under the unit declaration is what we're looking for
                        for LineNumber, StackLine in enumerate(showversion.splitlines()):
                            if f'UNIT {ModuleEntry}: SL 1' in StackLine:
                                Serial = re.search('(?<=#:).*', showversion.splitlines()[LineNumber + 1])
                                if Serial is not None:
                                    self.DeviceInfo['modules'][ModuleEntry]['serial'] = Serial.group().strip()
                                else:
                                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather serial- see debug logs!')
                                    OutputLog.debug(f'{self.BAPDID} - {showversion}')
                                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

                            if f'UNIT {ModuleEntry}: compiled' in StackLine:
                                Version = re.search('(?<=SW: Version ).*', showversion.splitlines()[LineNumber + 2])
                                if Version is not None:
                                    self.DeviceInfo['modules'][ModuleEntry]['version'] = Version.group().strip()
                                    self.DeviceInfo['summaryversion'] = Version.group().strip()
                                else:
                                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather version- see debug logs!')
                                    OutputLog.debug(f'{self.BAPDID} - {showversion}')
                                    self.DeviceInfo['ssh_reply'] = 'Incomplete'

            else:
                OutputLog.info(f'DID {self.BAPDID}- Device type not currently supported!')
                self.DeviceInfo['ssh_reply'] = 'Not_Supported'
                OutputLog.debug(f'DID {self.BAPDID} - {match_connection} - {postloginbanner}')
                device_connection.disconnect()
                return

        device_connection.disconnect()


if __name__ == "__main__":  # If the script is called manually, take the input as variables and run the function

    import pprint  # For readability
    from joblib import Parallel, delayed  # Joblib allows multithreading

    Targets = sys.argv[1]  # DIDs/accounts to handle
    RunOptions = sys.argv  # Catch all input to check for passed commands
    KeywordOptions = str([Option for Option in RunOptions if 'options=' in Option])

    # Check if a job number was provided- if not, default to -1 (number of local threads)
    if 'jobs=' in str(RunOptions):
        Jobs = int(re.search('(?<=jobs=)[0-9-]+', str(RunOptions)).group())
    else:
        Jobs = -1

    # Check if a sort key was provided- if not, default to SNMP/SSH
    SortKeys = [Option for Option in RunOptions if 'sort=' in Option]
    if len(SortKeys) == 0:
        SortKeys = ['snmp_reply', 'ssh_reply']
    else:
        SortKeys = SortKeys[0].replace("sort=", "").split(",")

    # Run the process and add the results to a list
    Results = Parallel(n_jobs=Jobs, verbose=10)(delayed(DeviceID().Handler)(TargetID, KeywordOptions) for TargetID in Targets.split(","))
    if 'hso' in KeywordOptions: Results = [Device for HSOList in Results for Device in HSOList]  # Each HSO would be a nested list- move the devices to the parent list

    # Sort the results- done in a try in case the fields aren't present or misspelled
    try:
        Results.sort(key=itemgetter(*SortKeys))
    except KeyError:
        OutputLog.error(f'Failed to sort by {SortKeys}- using SNMP/SSH instead')
        SortKeys = ['snmp_reply', 'ssh_reply']
        Results.sort(key=itemgetter(*SortKeys))

    # Check if a filter was provided
    FilterKeys = [Option for Option in RunOptions if 'filter=' in Option]
    if len(FilterKeys) > 0:
        FilterKeys = FilterKeys[0].replace("filter=", "").split(",")  # Split the string into a list of strings
        if 'bapdid' not in FilterKeys: FilterKeys.insert(0, 'bapdid')  # This is something that should always be present
        FilteredResults = []  # Create a temporary list for the results
        for Device in Results:
            FilteredResults.append({key: Device[key] for key in FilterKeys if key in Device})  # Create a dict item of the relevant key/value pairs and assign it to the temporary list

        Results = FilteredResults  # Replace the results with the filtered results

    # Print back the requisite info, prettyprint if not requested to be raw
    if 'raw' in KeywordOptions:
        for Device in Results:
            if "password" in Device:
                Device.pop("password", None)  # Remove the password from the output
            print(Device)
    else:
        pprint.pprint(Results)
