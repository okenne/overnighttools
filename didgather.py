#!/usr/bin/env python3

# Import a few prerequisites
from Subscripts import Logger  # Sets up logging
from Subscripts import ConfigLoad  # Loads the config file
from Subscripts import TimezoneOffset
import requests  # Gathering of API data from a URL
import json  # JSON allows for processing of JSON-formatted data
from joblib import Parallel, delayed  # Joblib allows multithreading

# The following items are for setting up file and console logging
import sys  # Script input call
import os  # OS allows us to determine the working path of the script for the config file

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


class DIDGather:

    def Handler(self, TargetID, GatherOptions):
        # Authentication information for the BAP API
        self.AuthParameters = {"access_token": ConfigFile['AuthToken'], "client_id": ConfigFile['ClientID']}

        # Split based on the option requested
        if 'hso' in GatherOptions:
            # Defining parameters here for easy modification
            DeviceTypes = "1,3,7,10,15,16,18,101,104,152,168,169,170,171,180,185,187,191,195,199,209,210,211,225,251,260"
            MaxReturn = "1000"
            IncludeVirtual = "false"

            # Format the HSO items and gather the info from the API
            HSOAPIParameters = {"access_token": ConfigFile['AuthToken'], "client_id": ConfigFile['ClientID'], "maxReturn": MaxReturn, "types": DeviceTypes, "hsoid": TargetID, "includeVirtual": IncludeVirtual}
            HSOAPIUrl = 'https://api.aws.opennetworkexchange.net/api/v2/devices'
            HSOInfoRaw = requests.get(HSOAPIUrl, params=HSOAPIParameters)
            HSOInfoJSON = HSOInfoRaw.json()

            # Make sure that the gathered information includes a typical field, as status codes are fallible (200 OK could be blank)
            if 'totalCount' not in HSOInfoJSON:
                # If the attempt to gather the info failed, write the failed string and the status code to file
                OutputLog.error(f'HSO {str(TargetID)}- Cannot gather information for this HSO! Check the debug logs for the string!')
                OutputLog.debug(f'HSO {str(TargetID)}- {str(HSOInfoRaw.status_code)} from {str(HSOInfoRaw.url)}\n')
                return

            DeviceList = {}  # Define an empty list to add the results to
            # Most of the processing is defined as a function to allow "return" to break the individual DIDs
            DeviceList = Parallel(n_jobs=10, verbose=0)(delayed(self.Process)(DIDEntry, GatherOptions) for DIDEntry in HSOInfoJSON['devices'])  # We have to process the DIDs to gather the management interface items
            DeviceList = list(filter(None.__ne__, DeviceList))  # Strip out the None entries from failed entries
            return DeviceList
            
        else:
            # Get the base information for the DID from the BAP API, and parse it as JSON
            APIInfo = requests.get('https://api.aws.opennetworkexchange.net/api/v2/devices/{0}'.format(TargetID), params=self.AuthParameters)
            APIInfoJSON = APIInfo.json()

            # Make sure that the gathered information includes a typical field, as status codes are fallible (200 OK could be blank)
            if 'adminPort' in APIInfoJSON:
                return self.Process(APIInfoJSON, GatherOptions)
            else:
                # If the attempt to gather the info failed, write the failed string and the status code to file
                OutputLog.error(f'DID {str(TargetID)}- Cannot gather information for this device! Check the debug logs for the string!')
                OutputLog.debug(f'{str(TargetID)}- {str(APIInfo.status_code)} from {str(APIInfo.url)}\n')
                return

    def Process(self, APIInfoJSON, GatherOptions):
        # We'll start with a blank dict object to assign the results to
        DeviceInfo = {'bapdid': APIInfoJSON['id'], 'hsoid': APIInfoJSON['hsoid'], 'snmpcommunity': APIInfoJSON['ro'],
                      'snmpport': APIInfoJSON['snmpPort']}

        # Get the timezone for the device
        DeviceInfo['timezone'] = TimezoneOffset(DeviceInfo['hsoid'])

        # Parse out some information
        BAPDID = str(DeviceInfo['bapdid'])  # Create a reference for the logger
        # If the port is 0, check the monitoring zone and set to 161 if not on a local poller
        if DeviceInfo['snmpport'] == 0:
            if 'AWS-US' in APIInfoJSON['monitoringZone']:
                DeviceInfo['snmpport'] = 161
            else:
                # Set the SNMP port based on standards
                DeviceInfo['snmpport'] = '5'+str(DeviceInfo['bapdid'])[-4:]
                
        DeviceInfo['baphostname'] = APIInfoJSON['name']
        # If the hostname is blank, use the NASID
        if DeviceInfo['baphostname'] == '':
            DeviceInfo['baphostname'] = APIInfoJSON['nasid']

        DeviceInfo['bapmonitortype'] = APIInfoJSON['statusType']
        DeviceInfo['baplaststatus'] = APIInfoJSON['statusLastStatus']
        DeviceInfo['ipaddress'] = APIInfoJSON['publicIp']

        # If the DID info didn't include a public IP, get information from the management parent device
        if DeviceInfo['ipaddress'] == "":
            ParentAPIInfo = requests.get('https://api.aws.opennetworkexchange.net/api/v2/devices/{0}'.format(APIInfoJSON['managementParent']), params=self.AuthParameters)

            # Make sure that the gathered information includes a typical field
            if "publicIp" in ParentAPIInfo.json():
                DeviceInfo['ipaddress'] = ParentAPIInfo.json()['publicIp']

                # If for some reason the management parent device does not have a public IP, error
                if DeviceInfo['ipaddress'] == "":
                    OutputLog.info(f'DID {BAPDID} does not have a management parent with a public IP address- will not continue on this node')
                    return
            else:
                # If there is no management parent, error
                OutputLog.info(f'DID {BAPDID} does not have a valid management parent- will not continue on this node')
                return
                
        # If the monitoring type is 0, then the device is not monitored and probably not accessible. We will skip these devices if not told to continue via "mon"
        if (DeviceInfo['bapmonitortype'] == 0) and ("mon" not in GatherOptions):
            OutputLog.info(f'DID {BAPDID} is not monitored- skipping!')
            return

        # Get the management information for the DID from the BAP API, and parse it as JSON
        APIMGMTInfo = requests.get('https://api.aws.opennetworkexchange.net/api/v2/managementinterfaces/devices/{0}'.format(DeviceInfo['bapdid']), params=self.AuthParameters)
        APIMGMTInfoJSON = APIMGMTInfo.json()

        # Make sure that the gathered information includes a typical field, as status codes are fallible (200 OK could be blank)
        if 'deviceId' in str(APIMGMTInfoJSON):

            # Parse out some information- we'll need to do this as a loop as there are multiple items
            for ManageEntry in APIMGMTInfoJSON['managementInterfaces']:
                # Check if there is an HTTPS forward and document this
                if ManageEntry['protocol'] == 2:
                    DeviceInfo['httpsport'] = ManageEntry['adminPort'] or "443"

                # We only care about SSH devices, or if none are listed assume port 22
                if ManageEntry['protocol'] == 3:
                    # Parse the info, default to SD standard credentials
                    DeviceInfo['username'] = ManageEntry['adminUsername'] or ConfigFile['StandardUsername']
                    DeviceInfo['password'] = ManageEntry['adminPassword'] or ConfigFile['StandardPassword']
                    DeviceInfo['adminport'] = ManageEntry['adminPort'] or "22"
                    DeviceInfo['adminproto'] = 3
                    TACACSState = ManageEntry['tacacs']
                    if TACACSState or ('tacacs' in DeviceInfo['password'].lower()):
                        DeviceInfo['username'] = ConfigFile['TACACSUsername']
                        DeviceInfo['password'] = ConfigFile['TACACSPassword']
                elif 'ParentAPIInfo' in vars():
                    pass  # We cannot assume port info if there is a management parent
                elif 'username' in DeviceInfo:
                    pass  # Skip if there's already an entry
                else:
                    DeviceInfo['username'] = ManageEntry['adminUsername'] or ConfigFile['StandardUsername']
                    DeviceInfo['password'] = ManageEntry['adminPassword'] or ConfigFile['StandardPassword']
                    DeviceInfo['adminport'] = "22"
                    DeviceInfo['adminproto'] = 3
                    TACACSState = ManageEntry['tacacs']
                    if TACACSState or ('tacacs' in DeviceInfo['password'].lower()):
                        DeviceInfo['username'] = ConfigFile['TACACSUsername']
                        DeviceInfo['password'] = ConfigFile['TACACSPassword']

        else:
            print(APIMGMTInfoJSON)
            # Some devices don't have a management entry, so the table replies as blank.  This is a catch for that
            if 'ParentAPIInfo' in vars():
                pass
            else:
                DeviceInfo['username'] = ConfigFile['TACACSUsername']
                DeviceInfo['password'] = ConfigFile['TACACSPassword']
                DeviceInfo['adminport'] = "22"
                DeviceInfo['adminproto'] = 3

        # Making sure a management entry is present after the above
        if DeviceInfo.get('adminport', -1) != -1:
            pass
        else:
            OutputLog.info(f'DID {BAPDID}- Cannot gather management information for this device (no SSH port forward)! Check the debug logs for the string!')
            OutputLog.debug(f"{str(DeviceInfo['bapdid'])}- {str(APIMGMTInfo.status_code)} from {str(APIMGMTInfo.url)}\n")
        
        return DeviceInfo


# If the script is called manually, take the input as variables and run the function
if __name__ == "__main__":

    import pprint  # For readability

    # Import a prerequisite to allow gathering variables from script input
    Target = sys.argv[1]
    try:
        RunOptions = sys.argv[2]
    except IndexError:
        RunOptions = ""
    
    Results = DIDGather().Handler(Target, RunOptions)

    # Print back the requisite info
    pprint.pprint(Results)
