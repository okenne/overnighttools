#!/usr/bin/env python3

from didgather import DIDGather  # DIDgather will allow us to gather prerequisite information from the BAP
from deviceid import DeviceID  # DeviceID gets basic information from devices
from Subscripts import SSHSetup  # Used to set up Netmiko SSH sessions to specified devices
from Subscripts import TimeCalculate  # Determines reload time in minutes or hours
from Subscripts import Logger  # Sets up logging
from Subscripts import ConfigLoad  # Loads the config file

# The following items are for setting up file and console logging
import sys  # Script input call
import os  # Assists with filesystem items

import time  # For sleep functions
import re  # Regular expressions for text operations
# from collections import Counter  # Allows easy counting of AP items
from pkg_resources import parse_version  # Allows for software version comparison
import signal  # Handles keypress items such as CTRL+C
import shutil  # Copying files from one location to another

import yaml  # Used to parse the approved firmware file
from io import BytesIO  # Used to convert strings to a file-like object for FTP uploads

from pexpect import *  # Used for SFTP connections to Nomadix gateways
import paramiko  # Used for other SFTP sessions
from tempfile import NamedTemporaryFile  # Used for Pexpect SFTP file downloads to a temporary file
from subprocess import Popen  # Span background tasks for devices that can't time a reload on their own
import json  # Encoding and decoding JSON structures
from scp import SCPClient, SCPException  # Used for copying files to HP switches
import requests  # Used for downloading backup files from ZoneDirectors

OutputLog = Logger(os.path.basename(sys.argv[0]))  # Define the logger based on the script name
ConfigFile = ConfigLoad()  # Load the config file


def signal_handler(sig, frame):
    OutputLog.error("CTRL+C pressed- cleaning up background items")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


# Need to gather the SFTP server info and the approved firmware file
# The SFTP server info is in DID 696304
SFTPServerInfo = DIDGather().Handler(696304, 'mon')

# Catch running options here

if "config" in sys.argv:
    ConfigOnly = True
else:
    ConfigOnly = False

if "abort" in sys.argv:
    Abort = True
else:
    Abort = False

# We only need the approved file if we are scheduling devices, so we will only get that file here
try:
    if ":" in sys.argv[2]:
        ReloadTime = sys.argv[2]
        ConfigOnly = False
        Abort = False
        # Log into the SFTP server
        Servertransport = paramiko.Transport((SFTPServerInfo['ipaddress'], SFTPServerInfo['adminport']))
        Servertransport.connect(username=SFTPServerInfo['username'], password=SFTPServerInfo['password'])
        ServerSFTPConnection = paramiko.SFTPClient.from_transport(Servertransport)
        ServerSFTPConnection = Servertransport.open_sftp_client()

        # Gather the contents of the approved firmware file into a file object, then parse as YAML into a dict
        ServerSFTPConnection.chdir('Firmware/')
        ApprovedFirmwareContents = BytesIO()
        ServerSFTPConnection.getfo('approvedfirmware.yaml', ApprovedFirmwareContents)
        ApprovedFirmware = yaml.load(ApprovedFirmwareContents.getvalue(), Loader=yaml.FullLoader)
        ServerSFTPConnection.close()  # Close the connection

except IndexError:
    ReloadTime = ""
    ConfigOnly = True
    Abort = False

# We can also save the configs locally- defined here
if "local" in sys.argv:
    LocalSave = True
else:
    LocalSave = False


def PExpectSFTPSetup(TargetInfo):

    BAPDID = str(TargetInfo['bapdid'])  # Defined for log reference
    # This is primarily for Nomadix devices
    # PExpect is necessary due to compatibility issues with most SFTP libraries and the VXWorks server implementation
    device_connection = spawn('sftp -o StrictHostKeyChecking=no -o Port=%s %s@%s' % (TargetInfo['adminport'], TargetInfo['username'], TargetInfo['ipaddress']))
    try:
        device_connection.expect('(?i)password:')
        CommandOutput = device_connection.sendline(TargetInfo['password'])
        CommandOutput = device_connection.expect(['Permission denied', 'sftp>'])
        if CommandOutput == 0:
            OutputLog.error(f'DID {BAPDID}- SFTP connection failed- incorrect password. Check the debug logs!')
            OutputLog.debug(TargetInfo['password'])
            device_connection.kill(0)
            return False
        else:
            # Catch the initial replies to avoid affecting the later results
            time.sleep(1)
            return device_connection
    except EOF:
        OutputLog.error(f'DID {BAPDID}- SFTP is disabled or connection failed due to premature end of file. Check the debug logs!')
        OutputLog.debug(str(device_connection))
        return False
    except TIMEOUT:
        OutputLog.error(f'DID {BAPDID}- SFTP connection failed due to timeout. Check the debug logs!')
        OutputLog.debug(str(device_connection))
        return False


def SFTPFolderValidate(SFTPConn, Directory):
    # Recurse through the directory presented to create folders as necessary
    if Directory != "":
        try:
            SFTPConn.chdir(Directory)
        except IOError:
            ParentDirectory, SubDirectory = os.path.split(Directory.rstrip('/'))
            SFTPFolderValidate(SFTPConn, ParentDirectory)
            OutputLog.debug(f'Making subdirectory {SubDirectory} on the server')
            SFTPConn.mkdir(SubDirectory)
            SFTPConn.chdir(SubDirectory)


def SFTPSetup(TargetInfo):
    # Create an SFTP connection to the requested device for further use
    BAPDID = str(TargetInfo['bapdid'])  # Defined for log reference
    try:
        transport = paramiko.Transport((TargetInfo['ipaddress'], TargetInfo['adminport']))
        transport.connect(username=TargetInfo['username'], password=TargetInfo['password'])
        SFTPConnection = paramiko.SFTPClient.from_transport(transport)
        SFTPConnection = transport.open_sftp_client()
    except:
        OutputLog.error(f'DID {BAPDID}- SFTP connection failed. Check the debug logs!')
        OutputLog.debug(f'DID {BAPDID}- {str(transport)} {str(SFTPConnection)}')

    return SFTPConnection


def SFTPUpload(TargetInfo, SourceFile, FileName, Method):
    # Upload the file presented to the specified directory on the FTP server (or save locally if requested)

    # Determine the filename and folder to upload the file to
    FileName = f'{FileName}-{str(TargetInfo["bapdid"])}-{TargetInfo["baphostname"]}-{time.strftime("%Y-%m-%d_%H-%M")}.txt'
    DeviceFolder = f"Configuration backups/{TargetInfo['hsoid']}/{TargetInfo['baphostname']}"

    # Convert the source into a bytes object if it is text
    if Method == "text":
        SourceFile = BytesIO(SourceFile.encode())

    if LocalSave:
        PathedFile = f'{DeviceFolder}/{FileName}'  # Combine the file names
        os.makedirs(DeviceFolder, exist_ok=True)

        OutputLog.info(f'{TargetInfo["bapdid"]}- Saving the file {FileName} locally')
        if Method == "text":
            with open(PathedFile, 'wb') as File:
                File.write(SourceFile.read())
        elif Method == "file":
            with open(PathedFile, 'wb') as File:
                File.write(SourceFile.read())
        elif Method == "path":
            shutil.copyfile(str(SourceFile.name), PathedFile)

    else:
        # Create the connection
        SFTPConn = SFTPSetup(SFTPServerInfo)
        # Validate or create the folder as necessary
        SFTPFolderValidate(SFTPConn, DeviceFolder)

        # Determine the method to use to upload the file and proceed. If the source file type supports providing the file size, get that
        try:
            OutputLog.info(f'{TargetInfo["bapdid"]}- Uploading the file {FileName} to SFTP, ({len(SourceFile.getvalue())} bytes)')
        except AttributeError:
            OutputLog.info(f'{TargetInfo["bapdid"]}- Uploading the file {FileName} to SFTP')

        if Method == "text" or "file":
            SFTPConn.putfo(SourceFile, FileName)
        elif Method == "path":
            SFTPConn.put(SourceFile, FileName)

        SFTPConn.close()

    time.sleep(2)  # Sleep after the connection to slow down concurrent connections


def ControllerScheduleHops(TargetInfo, TargetFirmware, CurrentVersion, ZDSeries, ControllerReloadTime, NetmikoProfile):
    # Define the BAPDID locally for log reference
    BAPDID = str(TargetInfo['bapdid'])

    # Check if there are any required versions, or if we are above the required version
    if TargetFirmware['required'] == 'none':
        OutputLog.info(f'DID {BAPDID}- no hops needed')
    elif parse_version(TargetInfo['summaryversion']) >= parse_version(TargetFirmware['required']['version']):
        OutputLog.info(f"DID {BAPDID}- device is on {TargetInfo['summaryversion']}, which is above the required version {TargetFirmware['required']['version']} for {TargetFirmware['version']}")
        pass
    else:
        # We need another hop first
        HopTarget = TargetFirmware['required']
        ControllerReloadTime, Error = ControllerScheduleHops(TargetInfo, HopTarget, CurrentVersion, ZDSeries, ControllerReloadTime, NetmikoProfile)
        if Error:
            OutputLog.error(f'DID {BAPDID}- failed to schedule {HopTarget["version"]}- canceling further hops!')
            return ControllerReloadTime, Error
        ControllerReloadTime = ControllerReloadTime + 1800
        CurrentVersion = TargetFirmware['required']['version']

    # Convert the device dict to a JSON
    TargetJSON = json.dumps(TargetInfo)

    # Construct a second dict for the information necessary to download the firmware, then convert that to JSON
    UpgradeInfo = {'FTPServerUsername': SFTPServerInfo['username'],
                   'FTPServerPassword': SFTPServerInfo['password'],
                   'FTPServerAddress': SFTPServerInfo['ipaddress'],
                   'OldVersion': CurrentVersion,
                   'FilePath': TargetFirmware[ZDSeries]['filepath'],
                   'FileName': TargetFirmware[ZDSeries]['filename']}
    UpgradeJSON = json.dumps(UpgradeInfo)

    # Spawn the background process
    OutputLog.info(f'DID {BAPDID}- scheduling the device for upgrade to {TargetFirmware["version"]} in {ControllerReloadTime} seconds')
    DeviceUpgrade = Popen(['python3', '__zonedirectorupgrade__.py', str(BAPDID), str(ControllerReloadTime), TargetJSON, NetmikoProfile, UpgradeJSON], cwd=f'{os.path.dirname(os.path.realpath(__file__))}/Subscripts')

    # Check to see if the process spawned correctly
    time.sleep(1)
    try:
        DeviceUpgrade.pid
        Error = False
    except AttributeError:
        OutputLog.error(f'DID {BAPDID}- background process failed to start!')
        Error = True

    return ReloadTime, Error


def BackgroundProcessCancel(BAPDID):
    OutputLog.warn(f'DID {BAPDID}- Requested to cancel pending background upgrades for this device!')
    os.system(f"pgrep -f '\"bapdid\": {BAPDID}'|xargs kill -9")


class DeviceUpdate:

    def Handler(self, TargetInfo):
        # Check to see if the input is pre-gathered ID data or if it is just a device ID
        if "bapdid" not in TargetInfo:
            try:
                if Abort:
                    TargetInfo = DeviceID().Handler(TargetInfo, "stat")  # If we are cancelling an update, ignore any device status
                else:
                    TargetInfo = DeviceID().Handler(TargetInfo, "")
                # Define the BAPDID locally for log reference
                self.BAPDID = str(TargetInfo['bapdid'])
            except:
                return f'{TargetInfo}- DeviceID failed- cannot proceed!'

        # Create a blank entry for status results
        self.DeviceStatus = {'bapdid': self.BAPDID}
        self.DeviceSummary = {'bapdid': self.BAPDID}

        # Check to see if the device previously failed SSH connections
        if 'ssh_reply' in TargetInfo:
            if TargetInfo['ssh_reply'] == 'Complete':
                pass
            elif TargetInfo['ssh_reply'] == 'Skipped':
                pass
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to reply properly to SSH during device ID- cannot proceed on this device!')
                self.DeviceStatus['configbackup'] = False
                return self.DeviceStatus
        elif 'adminport' not in TargetInfo:
            OutputLog.error(f'DID {self.BAPDID}- no admin port for this device- cannot proceed!')
            self.DeviceStatus['configbackup'] = False
            return self.DeviceStatus

        # Run these sections in a try so if a single section fails it won't affect the loop if present
        try:
            if Abort:  # If requested to abort, the rest is not relevant
                self.CancelAndReset(TargetInfo)
                self.DeviceSummary['lastsuccessfulstate'] = 'Cancelled and state reset'
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

            # First, let's backup the config
            self.ConfigBackup(TargetInfo)

            # Next, gather base info
            if self.DeviceStatus['configbackup']:
                self.DeviceSummary['lastsuccessfulstate'] = 'Configuration backed up'
                if ConfigOnly:
                    OutputLog.error(f'DID {self.BAPDID}- Config backup only- backed up the config')
                    self.DeviceStatus['infogathered'] = False
                    if 'full_output' in sys.argv:
                        return self.DeviceStatus
                    else:
                        return self.DeviceSummary
                else:
                    self.GatherDeviceInfo(TargetInfo)
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to back up config!')
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

            # Prepare the unit by cleaning up old images and enabling file transfer services if needed (if a reload time is specified)
            if self.DeviceStatus['infogathered']:
                self.DeviceSummary['lastsuccessfulstate'] = 'Info gathered'
                if ReloadTime:
                    self.ProcessandPrepare(TargetInfo)
                else:
                    OutputLog.error(f'DID {self.BAPDID}- No reload time specified- backed up the config')
                    if 'full_output' in sys.argv:
                        return self.DeviceStatus
                    else:
                        return self.DeviceSummary
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to gather info!')
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

            # Select and load the image
            if self.DeviceStatus['prepared']:
                self.DeviceSummary['lastsuccessfulstate'] = 'Preparation completed'
                self.LoadImage(TargetInfo)
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to prepare device!')
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

            # Set the boot, determine the reload time and schedule
            if self.DeviceStatus['loaded']:
                self.DeviceSummary['lastsuccessfulstate'] = 'Image loaded'
                self.SetAndSchedule(TargetInfo)
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to load device!')
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

            if self.DeviceStatus['scheduled']:
                self.DeviceSummary['lastsuccessfulstate'] = 'Reboot scheduled'
            else:
                OutputLog.error(f'DID {self.BAPDID}- failed to schedule reload!')
                if 'full_output' in sys.argv:
                    return self.DeviceStatus
                else:
                    return self.DeviceSummary

        except Exception as exception:
            ErrorMessage = f'DID {self.BAPDID}- failed due to exception {exception}!'
            OutputLog.error(ErrorMessage)
            return ErrorMessage

        if 'full_output' in sys.argv:
            return self.DeviceStatus
        else:
            return self.DeviceSummary

    def ConfigBackup(self, TargetInfo):

        self.DeviceStatus['configbackup'] = False

        # Select the commands for configuration items
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            self.NetmikoProfile = 'ruckus_fastiron'
            showconfig = "show running-config"

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('ASR[0-9]{4}|CISCO[0-9]{4}|WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                self.NetmikoProfile = 'cisco_ios'
                showconfig = "show running-config"
            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                self.NetmikoProfile = 'cisco_wlc_ssh'
                showconfig = "show run-config"
            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                self.NetmikoProfile = 'cisco_asa_ssh'
                showconfig = "more system:running-config"
            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')

        elif TargetInfo['vendor'] == 'Extreme':
            self.NetmikoProfile = 'extreme_exos'
            showconfig = "show configuration"

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            self.NetmikoProfile = 'hp_procurve'
            showconfig = "show running-config"

        elif TargetInfo['vendor'] == 'Mikrotik':
            self.NetmikoProfile = 'mikrotik_routeros'
            showconfig = "export"  # This command has a momentary pause in output that Netmiko interprets as the end of the command, so we have to use send_command_timing
            # Create the connection and run the relevant command if the connection established properly
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather the config')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

            if device_connection:
                DeviceRunningConfig = device_connection.send_command_timing(showconfig)
                device_connection.disconnect()
            else:
                self.DeviceStatus['configbackup'] = False
                return

            # Write the config to SFTP
            SFTPUpload(TargetInfo, DeviceRunningConfig, "running-config", "text")
            self.DeviceStatus['configbackup'] = True
            return

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            self.NetmikoProfile = 'extreme_wing_ssh'
            showconfig = "show running-config"

        elif TargetInfo['vendor'] == 'Nomadix':
            self.NetmikoProfile = 'generic_termserver'
            TargetFiles = ['current.txt', 'inatconf.txt', 'netconf.txt', 'nseconf.txt', 'RoomFileV2.txt', 'wanconf.txt']  # List of files to back up
            FileMap = {}  # Create a blank dict that will be used to match the file names to temporary paths

            for FileName in TargetFiles:
                FileMap[FileName] = NamedTemporaryFile()  # Create a temp file reference per file above

            # Start the connection
            OutputLog.info(f'DID {self.BAPDID}- creating SFTP session using pexpect to gather the config')
            device_connection = PExpectSFTPSetup(TargetInfo)

            # If the connection started correctly, proceed
            if device_connection:
                # For each of the files above, copy this file out
                for FileName in TargetFiles:
                    CommandOutput = device_connection.sendline('get ' + FileName + ' ' + FileMap[FileName].name)
                    CommandOutput = device_connection.expect('sftp>')
                CommandOutput = device_connection.isalive()
                CommandOutput = device_connection.close()
            else:
                self.DeviceStatus['configbackup'] = False
                return

            # Upload the file and close the connection
            OutputLog.info(f'DID {self.BAPDID}- Uploading the config to FTP')
            for FileName in TargetFiles:
                SFTPUpload(TargetInfo, FileMap[FileName], FileName, "path")
                FileMap[FileName].close()

            self.DeviceStatus['configbackup'] = True
            return

        elif TargetInfo['vendor'] == 'Ruckus':
            self.NetmikoProfile = 'generic_termserver'
            if 'ZD' in str(TargetInfo['modules']['1']['model']):
                self.DeviceStatus['type'] = 'ZoneDirector'
                showconfig = "show config"

                # The above provides a text-readable form of the configuration, but cannot be restored to the device
                # A backup file can be obtained in CLI, but only exported to TFTP. It is also available in the GUI, which we will gather via the HTTPS admin port
                login_url = f"https://{TargetInfo['ipaddress']}:{TargetInfo['httpsport']}/admin/login.jsp"
                config_url = f"https://{TargetInfo['ipaddress']}:{TargetInfo['httpsport']}/admin/_savebackup.jsp"

                login_payload = {"username": TargetInfo['username'], "password": TargetInfo['password'], "ok": ""}  # Login info for the device

                session_requests = requests.session()  # Create a session object so the login is persistent
                loginresult = session_requests.post(login_url, data=login_payload, verify=False)  # Verify is false to allow connections to self signed certs

                config = session_requests.get(config_url, verify=False)  # Navigate to the config URL, which will prompt to download
                config.raw.decode_content = True

                configfile = BytesIO(config.content)  # We must convert this into a file object to upload
                SFTPUpload(TargetInfo, configfile, f'{TargetInfo["modules"]["1"]["version"].replace(" ","_").replace(".","_")}-config.bak', "file")

        else:
            OutputLog.error(f'DID {self.BAPDID} is not currently supported!')
            return False

        # Create the connection and run the relevant command if the connection established properly
        OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather the config')
        device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

        if device_connection:
            DeviceRunningConfig = device_connection.send_command(showconfig)
            device_connection.disconnect()
        else:
            self.DeviceStatus['configbackup'] = False
            return

        # Write the config to SFTP
        SFTPUpload(TargetInfo, DeviceRunningConfig, "running-config", "text")

        self.DeviceStatus['configbackup'] = True
        OutputLog.info(f'DID {self.BAPDID}- configuration backup successful')

    def GatherDeviceInfo(self, TargetInfo):

        # Select the commands for gathering info
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            # NetmikoProfile = 'ruckus_fastiron'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                self.NetmikoProfile = 'cisco_ios'

                # If the target is a stack, make sure the versions match to begin with
                Versions = []
                for module in TargetInfo['modules']:
                    if 'version' in str(module):
                        Versions.append(module['version'])
                if all(version == Versions[0] for version in Versions):
                    pass
                else:
                    OutputLog.error(f'DID {self.BAPDID}- switch versions do not match to start- cannot proceed on this device!')
                    OutputLog.debug(f'{self.BAPDID} - {Versions}')
                    self.DeviceStatus['infogathered'] = False
                    return

                # Create the connection and run the relevant commands
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather info')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                showversion = device_connection.send_command('show version')
                showboot = device_connection.send_command('show boot system')
                showfilesystems = device_connection.send_command('show file systems | inc flash[-:0-9]')
                showreload = device_connection.send_command('show reload')

                CurrentImage = re.search('(?<=System image file is ").*(?=")', showversion)
                if CurrentImage is not None:
                    self.DeviceStatus['currentimage'] = CurrentImage.group()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showversion}')
                    self.DeviceStatus['infogathered'] = False

                if 'BOOT variable =' in showboot:
                    BootVar = re.search('(?<=BOOT variable = ).*', showboot)
                    if BootVar is not None:
                        self.DeviceStatus['bootvar'] = BootVar.group()
                    else:
                        # Some switches don't have a boot image specified.  For these, let's have the script set it to the current imagefile
                        OutputLog.info(f'{self.BAPDID} - No boot var found, assuming current boot')
                        OutputLog.debug(f'{self.BAPDID} - {showboot}')
                        self.DeviceStatus['bootvar'] = f'{self.DeviceStatus["currentimage"]}'
                elif 'flash' in showboot:
                    self.DeviceStatus['bootvar'] = showboot
                else:
                    # Some switches don't have a boot image specified.  For these, let's have the script set it to the current imagefile
                    OutputLog.info(f'{self.BAPDID} - No boot var found, assuming current boot')
                    OutputLog.debug(f'{self.BAPDID} - {showboot}')
                    self.DeviceStatus['bootvar'] = f'{self.DeviceStatus["currentimage"]}'

                # We need to filter out which "flash" filesystems are present
                self.FlashInfo = {'filesystems': re.findall(r"flash[-0-9]*:", showfilesystems, re.M)}
                if len(self.FlashInfo['filesystems']) > 1:
                    # flash: is aliased to the active switch's flash in a stack, so if there is more than one entry listed we need to remove this
                    self.FlashInfo['filesystems'] = [filesystem for filesystem in self.FlashInfo['filesystems'] if filesystem != 'flash:' and filesystem != 'cat4000_flash:' and filesystem != 'slavecat4000_flash:']
                # Get the contents of these flash partitions
                for filesystem in self.FlashInfo['filesystems']:
                    flashcontents = device_connection.send_command(f'dir {filesystem}')

                    if 'Invalid input detected' in flashcontents:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather flash contents- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {flashcontents}')
                        self.DeviceStatus['infogathered'] = False
                    else:
                        self.FlashInfo[filesystem] = {'freespace': re.search('(?<=total \\().*(?= bytes free)', flashcontents).group(),
                                                      'contents': flashcontents}

                if 'Reload scheduled' in showreload:
                    self.DeviceStatus['reloadscheduled'] = True
                else:
                    self.DeviceStatus['reloadscheduled'] = False

            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                # NetmikoProfile = 'cisco_wlc_ssh'
                # showconfig = "show run-config"
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['infogathered'] = False

            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                self.NetmikoProfile = 'cisco_asa_ssh'
                # Create the connection and run the relevant commands
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather info')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                showversion = device_connection.send_command('show version')
                showboot = device_connection.send_command('show boot')
                showflash = device_connection.send_command('show flash')
                showreload = device_connection.send_command('show reload')

                CurrentImage = re.search('(?<=System image file is "disk0:/).*(?=")', showversion)
                if CurrentImage is not None:
                    self.DeviceStatus['currentimage'] = CurrentImage.group()
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showversion}')
                    self.DeviceStatus['infogathered'] = False

                BootVar = re.search('(?<=BOOT variable = ).*', showboot)
                if BootVar is not None:
                    self.DeviceStatus['bootvar'] = BootVar.group()
                else:
                    # Some ASAs don't have a boot image specified.  For these, let's have the script set it to the current imagefile
                    OutputLog.info(f'{self.BAPDID} - No boot var found, assuming current boot')
                    OutputLog.debug(f'{self.BAPDID} - {showboot}')
                    self.DeviceStatus['bootvar'] = f'disk0:/{CurrentImage}'

                CurrentBootVar = re.search('(?<=Current BOOT variable = ).*', showboot)
                if CurrentBootVar is not None:
                    self.DeviceStatus['currentbootvar'] = CurrentBootVar.group()
                else:
                    # Some ASAs don't have a boot image specified.  For these, let's have the script set it to the current imagefile
                    OutputLog.info(f'{self.BAPDID} - No current boot var found, assuming current boot')
                    OutputLog.debug(f'{self.BAPDID} - {showboot}')
                    self.DeviceStatus['currentbootvar'] = f'disk0:/{CurrentImage}'

                FreeSpace = re.search('(?<=total \\().*(?= bytes free)', showflash)
                if FreeSpace is not None:
                    self.DeviceStatus['freespace'] = FreeSpace.group()
                    self.FlashContents = showflash
                else:
                    OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                    OutputLog.debug(f'{self.BAPDID} - {showflash}')
                    self.DeviceStatus['infogathered'] = False

                if TargetInfo['failover']:  # Get the proper info for a secondary if present
                    OutputLog.info(f'DID {self.BAPDID}- Gathering info for the secondary device')
                    secondaryshowversion = device_connection.send_command('failover exec mate show version')
                    secondaryshowflash = device_connection.send_command('failover exec mate show flash')

                    SecondaryCurrentImage = re.search('(?<=System image file is "disk0:/).*(?=")', secondaryshowversion)
                    if SecondaryCurrentImage is not None:
                        self.DeviceStatus['secondarycurrentimage'] = SecondaryCurrentImage.group()
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information for the secondary- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {secondaryshowversion}')
                        self.DeviceStatus['infogathered'] = False

                    SecondaryFreeSpace = re.search('(?<=total \\().*(?= bytes free)', secondaryshowflash)
                    if SecondaryFreeSpace is not None:
                        self.DeviceStatus['secondaryfreespace'] = SecondaryFreeSpace.group()
                        self.SecondaryFlashContents = secondaryshowflash
                    else:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information for the secondary- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {secondaryshowflash}')
                        self.DeviceStatus['infogathered'] = False

                    if 'Reload scheduled' in showreload:
                        self.DeviceStatus['reloadscheduled'] = True
                    else:
                        self.DeviceStatus['reloadscheduled'] = False

                device_connection.disconnect()

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Extreme':
            # NetmikoProfile = 'extreme_exos'
            # showconfig = "show configuration"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            self.NetmikoProfile = 'hp_procurve'
            # Create the connection and run the relevant commands
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather info')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
            showversion = device_connection.send_command('show version')
            showflash = device_connection.send_command('show flash')
            showssh = device_connection.send_command('show ip ssh')
            showreload = device_connection.send_command('show reload after')
            showjob = device_connection.send_command('show job')

            VersionString = re.compile('[A-Z]{1,2}.[0-9]{1,2}.[0-9]{1,2}.*')  # Defined here for multiple references

            CurrentImage = re.search(VersionString, showversion)
            if CurrentImage is not None:
                self.DeviceStatus['currentimage'] = CurrentImage.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showversion}')
                self.DeviceStatus['infogathered'] = False

            SCPStatus = re.search('(?<=SecureCopyEnabled:).*', showssh.replace(" ", ""))
            if SCPStatus is not None:
                self.DeviceStatus['scpenabled'] = SCPStatus.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showssh}')
                self.DeviceStatus['infogathered'] = False

            # Split out the flash info
            for Line in showflash.splitlines():
                if "Primary Image" in Line:
                    self.DeviceStatus['primaryflash'] = re.search(VersionString, Line).group().strip()
                elif "Secondary Image" in Line:
                    self.DeviceStatus['secondaryflash'] = re.search(VersionString, Line).group().strip()
                elif "Default Boot" in Line:
                    self.DeviceStatus['bootvar'] = re.search('(?<=: ).*', Line).group().lower()
                elif "Current Boot" in Line:
                    self.DeviceStatus['bootvar'] = re.search('(?<=: ).*', Line).group().lower()

            if 'primaryflash' not in self.DeviceStatus.keys():
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information for the primary flash- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showflash}')
                self.DeviceStatus['infogathered'] = False
            if 'secondaryflash' not in self.DeviceStatus.keys():
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information for the secondary flash- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showflash}')
                self.DeviceStatus['infogathered'] = False
            if 'bootvar' not in self.DeviceStatus.keys():
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information for the boot var- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {showflash}')
                self.DeviceStatus['infogathered'] = False

            # Let's parse the flash image that is active or set to next boot
            self.DeviceStatus['activeflash'] = str(self.DeviceStatus[f"{self.DeviceStatus['bootvar']}flash"])

            if 'Reload scheduled' in showreload:
                self.DeviceStatus['reloadscheduled'] = True
            else:
                if 'boot system' in showjob:
                    self.DeviceStatus['reloadscheduled'] = True
                else:
                    self.DeviceStatus['reloadscheduled'] = False

            device_connection.disconnect()

        elif TargetInfo['vendor'] == 'Mikrotik':
            self.NetmikoProfile = 'mikrotik_routeros'
            # Create the connection and run the relevant commands
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather info')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
            device_connection.send_command('system package update check-for-updates once')
            packageupdatecheck = device_connection.send_command('system package update print')

            UpdateChannel = re.search('(?<=channel: ).*', packageupdatecheck)
            if UpdateChannel is not None:
                self.DeviceStatus['updatechannel'] = UpdateChannel.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {packageupdatecheck}')
                self.DeviceStatus['infogathered'] = False

            CheckUpdateStatus = re.search('(?<=status: ).*', packageupdatecheck)
            if CheckUpdateStatus is not None:
                self.DeviceStatus['checkupdatestatus'] = CheckUpdateStatus.group().strip()
            else:
                OutputLog.warning(f'{self.BAPDID} - Unable to properly gather information- see debug logs!')
                OutputLog.debug(f'{self.BAPDID} - {packageupdatecheck}')
                self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            # NetmikoProfile = 'extreme_wing_ssh'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False

        elif TargetInfo['vendor'] == 'Nomadix':
            # We need to check the flash contents to determine if there are firmware packages already present

            # Start the connection
            OutputLog.info(f'DID {self.BAPDID}- creating SFTP session using pexpect to gather the info')
            device_connection = PExpectSFTPSetup(TargetInfo)

            # If the connection started correctly, proceed
            if device_connection:
                # LS -1 will only list the file names
                CommandOutput = device_connection.sendline('ls -1 firmware*')
                time.sleep(1)
                CommandOutput = device_connection.expect('sftp>')
                self.FlashContents = device_connection.before.decode('utf-8')
                CommandOutput = device_connection.isalive()
                CommandOutput = device_connection.close()
            else:
                self.DeviceStatus['infogathered'] = False
                return

        elif TargetInfo['vendor'] == 'Ruckus':
            if self.DeviceStatus['type'] == 'ZoneDirector':
                # We need to check if the unit displays a support contract

                # Start the connection
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to gather info')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                # Navigate to the menu
                device_connection.send_command('config', expect_string=r'(config)')
                device_connection.send_command('system', expect_string=r'(config-sys)')

                device_connection.send_command('support-entitle')
                supportcheck = device_connection.send_command('show support')

                SupportStatus = re.search('(?<=Status: ).*', supportcheck)
                if SupportStatus is not None:
                    self.DeviceStatus['supportstatus'] = SupportStatus.group().strip()
                else:
                    self.DeviceStatus['supportstatus'] = 'unknown'
                    OutputLog.warning(f'{self.BAPDID} - Unable to check support information- possible this unit is before 9.9. Check debug logs if this is not the case')
                    OutputLog.debug(f'{self.BAPDID} - {supportcheck}')

                device_connection.disconnect()
                
            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['infogathered'] = False

        else:
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['infogathered'] = False
            return False

        if 'infogathered' not in self.DeviceStatus.keys():
            self.DeviceStatus['infogathered'] = True

        OutputLog.info(f'DID {self.BAPDID}- information gathering successful')

    def ProcessandPrepare(self, TargetInfo):

        # Select the commands for processing items
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            # NetmikoProfile = 'ruckus_fastiron'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                # Check to make sure the device isn't pending a reboot
                if str(self.DeviceStatus['currentimage']) not in str(self.DeviceStatus['bootvar']):
                    OutputLog.error(f'DID {self.BAPDID}- this device is pending a reboot!')
                    OutputLog.debug(f"DID {self.BAPDID}- {self.DeviceStatus['currentimage']}- {self.DeviceStatus['bootvar']}")
                    self.DeviceStatus['prepared'] = False
                    return

                # Create the connection and run the relevant commands
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to prepare the device')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                if re.search('cat4500', str(self.DeviceStatus['currentimage'])) is not None:
                    OutputLog.error(f'{self.BAPDID} is not currently supported!')
                    self.DeviceStatus['prepared'] = False
                    device_connection.disconnect()
                    return

                # We need to determine the files to clean out. 3650 and 3850 switches require different handling due to the potential that the image is installed
                if re.search('WS-C3650|WS-C3850', str(TargetInfo)) is not None:
                    if 'packages.conf' in self.DeviceStatus['currentimage']:
                        OutputLog.info(f'DID {self.BAPDID}- this is a 3650/3850 booted into install- cleaning the switch')

                        # Determine the relevant command based on version- 16.x introduced new syntax
                        if parse_version(TargetInfo['summaryversion']) > parse_version('15.01'):
                            cleancommand = 'request platform software package clean switch all'
                        else:
                            cleancommand = 'software clean'

                        cleanresult = device_connection.send_command_timing(f'{cleancommand}')
                        if 'Do you want to proceed?' in cleanresult:
                            device_connection.send_command(f'yes', expect_string=r'iles deleted')
                        elif 'FAILED: Clean inactive packages: previous provisioning action is pending reboot':
                            OutputLog.error(f'{self.BAPDID} this appears to have been rolled back- please investigate!')
                            self.DeviceStatus['prepared'] = False
                            device_connection.disconnect()
                            return

                    elif '.bin' in self.DeviceStatus['currentimage']:
                        OutputLog.info(f'DID {self.BAPDID}- this is a 3650/3850 booted into bundle- deleting package files')
                        for filesystem in self.FlashInfo['filesystems']:
                            OutputLog.info(f'DID {self.BAPDID}- cleaning up {filesystem}')
                            device_connection.send_command(f'del /force {filesystem}cat3k_caa*.pkg')
                            device_connection.send_command(f'del /force {filesystem}packages.con*')
                    else:
                        OutputLog.error(f'{self.BAPDID} cannot determine boot type for a 3650/3850 and thus cannot safely delete images- please investigate!')
                        self.DeviceStatus['prepared'] = False
                        device_connection.disconnect()
                        return
                else:
                    # Treating as a general IOS switch
                    for filesystem in self.FlashInfo['filesystems']:
                        for fileline in self.FlashInfo[filesystem]['contents'].splitlines():
                            if re.search('c[23][0-9]{3}', str(fileline)) is not None:
                                File = re.search('c[23][0-9]{3}.*', fileline).group()
                                PathedFile = f'{filesystem}{File}'
                                if File in self.DeviceStatus['currentimage']:
                                    OutputLog.info(f'DID {self.BAPDID}- skipping {PathedFile} as this is the current image')
                                else:
                                    device_connection.send_command(f'delete /force /recursive {PathedFile}')
                                    self.DeviceStatus.setdefault('deletedimages', []).append(f'{PathedFile}')
                                    OutputLog.info(f'DID {self.BAPDID}- deleted {PathedFile}')

                # Refresh the free space and contents
                for filesystem in self.FlashInfo['filesystems']:
                    flashcontents = device_connection.send_command(f'dir {filesystem}')

                    if 'Invalid input detected' in flashcontents:
                        OutputLog.warning(f'{self.BAPDID} - Unable to properly gather flash contents- see debug logs!')
                        OutputLog.debug(f'{self.BAPDID} - {flashcontents}')
                        self.DeviceStatus['infogathered'] = False
                    else:
                        self.FlashInfo[filesystem] = {'freespace': re.search('(?<=total \\().*(?= bytes free)', flashcontents).group(),
                                                      'contents': flashcontents}

                device_connection.disconnect()

            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                # NetmikoProfile = 'cisco_wlc_ssh'
                # showconfig = "show run-config"
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['prepared'] = False

            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                # Check to make sure the ASA isn't pending a reboot
                if str(self.DeviceStatus['currentimage']) not in str(self.DeviceStatus['currentbootvar']).split(";")[0]:
                    OutputLog.error(f'DID {self.BAPDID}- this device is pending a reboot!')
                    OutputLog.debug(f"DID {self.BAPDID}- {self.DeviceStatus['currentimage']}- {self.DeviceStatus['currentbootvar']}")
                    self.DeviceStatus['prepared'] = False
                    return

                # Create the connection and run the relevant commands
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to prepare the device')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                # Delete the old images and remove them from the boot order
                for image in self.DeviceStatus['currentbootvar'].split(";")[1:]:
                    if 'disk' in image:
                        device_connection.send_command(f'delete /noconfirm {image}')
                        device_connection.send_config_set(f'no boot system {image}')
                        device_connection.send_command('write memory')

                        # Add the deleted imaged to a list for reporting
                        self.DeviceStatus.setdefault('deletedimages', []).append(image)

                        # Refresh the free space after deletion
                        showflash = device_connection.send_command('show flash')
                        FreeSpace = re.search('(?<=total \\().*(?= bytes free)', showflash)
                        if FreeSpace is not None:
                            self.DeviceStatus['freespace'] = FreeSpace.group()
                        else:
                            OutputLog.warning(f'{self.BAPDID} - Unable to refresh free space- see debug logs!')
                            OutputLog.debug(f'{self.BAPDID} - {showflash}')
                            self.DeviceStatus['prepared'] = False

                        if TargetInfo['failover']:  # Prepare the secondary if present
                            OutputLog.info(f'DID {self.BAPDID}- Preparing the secondary device')
                            device_connection.send_command(f'failover exec mate delete /noconfirm {image}')
                            secondaryshowflash = device_connection.send_command('failover exec mate show flash')

                            SecondaryFreeSpace = re.search('(?<=total \\().*(?= bytes free)', secondaryshowflash)
                            if SecondaryFreeSpace is not None:
                                self.DeviceStatus['secondaryfreespace'] = SecondaryFreeSpace.group()
                                self.SecondaryFlashContents = secondaryshowflash
                            else:
                                OutputLog.warning(f'{self.BAPDID} - Unable to refresh free space for the secondary- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {secondaryshowflash}')
                                self.DeviceStatus['prepared'] = False

                device_connection.disconnect()

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Extreme':
            # NetmikoProfile = 'extreme_exos'
            # showconfig = "show configuration"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            # Check to make sure the device isn't pending a reboot
            if str(self.DeviceStatus['currentimage']) not in str(self.DeviceStatus['activeflash']):
                OutputLog.error(f'DID {self.BAPDID}- the current image {self.DeviceStatus["currentimage"]} is not set to next boot- device may be pending reboot!')
                OutputLog.debug(f"DID {self.BAPDID}- {self.DeviceStatus['currentimage']}- {self.DeviceStatus['bootvar']}- {self.DeviceStatus['activeflash']}")
                self.DeviceStatus['prepared'] = False
                return

            # Check if SCP is enabled
            if "No" in self.DeviceStatus['scpenabled']:
                # Create the connection and run the relevant commands
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to enable SCP')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                device_connection.send_config_set('ip ssh filetransfer')
                device_connection.send_command('write memory')

                device_connection.disconnect()

        elif TargetInfo['vendor'] == 'Mikrotik':
            # Check the package channel first- we're looking for 'stable' or 'current'
            if ("stable" in self.DeviceStatus['updatechannel']) or ("current" in self.DeviceStatus['updatechannel']):
                pass
            else:
                OutputLog.error(f'{self.BAPDID}- update channel is {self.DeviceStatus["updatechannel"]}- only stable and current are supported!')
                self.DeviceStatus['prepared'] = False
                return

            # Check the result of the update check
            if "New version is available" in self.DeviceStatus['checkupdatestatus']:
                pass  # No further actions necessary
            elif "Downloaded, please reboot router to upgrade it" in self.DeviceStatus['checkupdatestatus']:
                pass  # No further actions necessary
            elif "System is already up to date" in self.DeviceStatus['checkupdatestatus']:
                OutputLog.error(f'{self.BAPDID}- device reports that it is up to date!')
                self.DeviceStatus['prepared'] = False
                return
            elif "finding out latest version..." in self.DeviceStatus['checkupdatestatus']:
                OutputLog.error(f'{self.BAPDID}- device failed to check for updates!')
                self.DeviceStatus['prepared'] = False
                return
            else:
                OutputLog.error(f'{self.BAPDID}- unable to determine update check status- reported as {self.DeviceStatus["checkupdatestatus"]}!')
                self.DeviceStatus['prepared'] = False
                return

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            # NetmikoProfile = 'extreme_wing_ssh'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False

        elif TargetInfo['vendor'] == 'Nomadix':
            # If there are update images currently present, these should be removed
            if "firmware_" in self.FlashContents:
                OutputLog.info(f'DID {self.BAPDID}- creating SFTP session using pexpect to delete old images {self.FlashContents}')
                device_connection = PExpectSFTPSetup(TargetInfo)

                # If the connection started correctly, proceed
                if device_connection:

                    # For each existing image, delete it
                    # There is an initial line for the command, so we will check each line to see if it has the file name
                    for File in self.FlashContents.splitlines():
                        if "firmware_" in File:
                            CommandOutput = device_connection.sendline('rm ' + File)
                            CommandOutput = device_connection.expect('sftp>')

                            OutputLog.info(f'DID {self.BAPDID}- Deleted {File}')

                    CommandOutput = device_connection.isalive()
                    CommandOutput = device_connection.close()

                else:
                    self.DeviceStatus['prepared'] = False
                    return

        elif TargetInfo['vendor'] == 'Ruckus':
            if self.DeviceStatus['type'] == 'ZoneDirector':
                # If the device has active support, this is all the preparation is needed
                if self.DeviceStatus['supportstatus'] == 'active':
                    pass
                elif self.DeviceStatus['supportstatus'] == 'unknown':
                    pass
                else:
                    OutputLog.error(f'{self.BAPDID} does not have a current support contract- cannot proceed on this unit!')
                    self.DeviceStatus['prepared'] = False

                # Updating the ZoneDirector from the CLI does not synchronize with the secondary- this should be performed manually
                if 'peer_state' in TargetInfo:
                    if TargetInfo['modules']['1']['peer_state'] == 'connected':
                        OutputLog.error(f'{self.BAPDID} is set up in Smart Redundancy- cannot proceed on this unit!')
                        self.DeviceStatus['prepared'] = False

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['prepared'] = False

        else:
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['prepared'] = False
            return False

        if 'prepared' not in self.DeviceStatus.keys():
            self.DeviceStatus['prepared'] = True

        OutputLog.info(f'DID {self.BAPDID}- preparation successful')

    def LoadImage(self, TargetInfo):

        # Select the commands for loading items
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            # NetmikoProfile = 'ruckus_fastiron'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                # We need to determine a model to use to determine the firmware, which may involve parsing the model
                FirmwareModel = ""
                for Module in TargetInfo['modules'].keys():
                    if 'version' in TargetInfo['modules'][Module].keys():
                        Model = re.search('(?<=WS-)C[A-Z0-9]*|X45-SUP[A-Z0-9]', TargetInfo['modules'][Module]['model'])
                        if Model is not None:
                            FirmwareModel = Model.group()

                if FirmwareModel is None:
                    OutputLog.error(f'{self.BAPDID} cannot determine a usable model!')
                    self.DeviceStatus['loaded'] = False
                    return

                self.TargetFirmware = ApprovedFirmware[TargetInfo['vendor']][FirmwareModel]

                # Make sure there's enough free space- in the case of a tar file this will be larger than the actual file size, but we do not copy the HTML files so it ends up similar
                for filesystem in self.FlashInfo['filesystems']:
                    if int(self.TargetFirmware['filesize']) < int(self.FlashInfo[filesystem]['freespace']):
                        OutputLog.info(f'{self.BAPDID} has enough space for the image')
                    else:
                        OutputLog.error(f"{self.BAPDID} doesn't have enough space for the image on {filesystem}- {self.TargetFirmware['filesize']} needed and {self.FlashInfo[filesystem]['freespace']} available!")
                        self.DeviceStatus['loaded'] = False
                        return

                # Load the image
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to load the image')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                try:
                    if self.TargetFirmware['method'] == 'archive':
                        OutputLog.info(f'DID {self.BAPDID}- using archive to load the image')
                        CopyResult = device_connection.send_command_timing(f"archive download-sw /imageonly /leave-old-sw ftp://{SFTPServerInfo['username']}:{SFTPServerInfo['password']}@{SFTPServerInfo['ipaddress']}{self.TargetFirmware['filepath']}{self.TargetFirmware['filename']}", delay_factor=300)
                        for retry in range(2):
                            if "All software images installed" in CopyResult:
                                OutputLog.info(f'DID {self.BAPDID}- loading complete')
                            elif "Image family mismatch" in CopyResult:
                                OutputLog.error(f'DID {self.BAPDID}- image family mismatch!')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            elif "There is insufficient space" in CopyResult:
                                OutputLog.error(f'DID {self.BAPDID}- insufficient space!')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            elif "is the same as an existing image" in CopyResult:
                                OutputLog.error(f'DID {self.BAPDID}- this is the same as an existing image!')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            else:
                                # It is possible that the device has slow flash and has paused output during extraction- wait a little and check
                                device_connection.send_command_timing("\n", delay_factor=1, max_loops=1)
                                time.sleep(300)
                                CopyResult = device_connection.read_channel()
                        else:
                            OutputLog.error(f'DID {self.BAPDID}- did not copy the file within the expected timing!')
                            self.DeviceStatus['loaded'] = False
                            device_connection.disconnect()
                            return
                    elif self.TargetFirmware['method'] == 'request':

                        if 'packages.conf' in self.DeviceStatus['currentimage']:
                            OutputLog.info(f'DID {self.BAPDID}- this is a 3650/3850 booted into install- copying the file')

                            device_connection.send_command(f"copy ftp://{SFTPServerInfo['username']}:{SFTPServerInfo['password']}@{SFTPServerInfo['ipaddress']}{self.TargetFirmware['filepath']}{self.TargetFirmware['filename']} flash:", expect_string=r'Destination filename')
                            copyresult = device_connection.send_command_timing('\n', delay_factor=300)

                            # Check the copy result
                            if 'Warning:There is a file already existing with this name' in copyresult:
                                OutputLog.error(f'DID {self.BAPDID}- this is the same as an existing image!')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            elif 'No space left on device' in copyresult:
                                OutputLog.error(f'DID {self.BAPDID}- insufficient space!')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            elif '[OK - ' in copyresult:
                                OutputLog.info(f'DID {self.BAPDID}- loading complete')
                            else:
                                OutputLog.error(f'DID {self.BAPDID}- did not copy the file within the expected timing- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {copyresult}')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return

                            # Determine the relevant command based on version- 16.x introduced new syntax
                            if parse_version(TargetInfo['summaryversion']) > parse_version('15.01'):
                                installcommand = f"request platform software package install switch all file flash:{self.TargetFirmware['filename']} on-reboot new auto-copy"
                            else:
                                installcommand = f"software install file flash:{self.TargetFirmware['filename']} new on-reboot"

                            installresult = device_connection.send_command_timing(installcommand, delay_factor=300)

                            if 'SUCCESS: Finished install' in installresult:
                                OutputLog.info(f'DID {self.BAPDID}- installation complete')
                            elif 'FAILED: ' in installresult:
                                OutputLog.error(f'DID {self.BAPDID}- did not install correctly- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {installresult}')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return
                            else:
                                OutputLog.error(f'DID {self.BAPDID}- did not install correctly- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {installresult}')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return

                        elif '.bin' in self.DeviceStatus['currentimage']:
                            # We need to expand the current image to boot into it
                            OutputLog.info(f'DID {self.BAPDID}- this is a 3650/3850 booted into bundle- expanding the package')

                            # Determine the relevant command based on version- 16.x introduced new syntax
                            if parse_version(TargetInfo['summaryversion']) > parse_version('15.01'):
                                installcommand = f'request platform software package expand switch all file {self.DeviceStatus["currentimage"]} retain-source-file overwrite'
                            else:
                                installcommand = 'software expand running to flash:'

                            installresult = device_connection.send_command(f'{installcommand}')

                            if 'Finished expanding' in installresult:
                                OutputLog.info(f'DID {self.BAPDID}- expansion complete')
                            else:
                                OutputLog.error(f'DID {self.BAPDID}- did not expand correctly- see debug logs!')
                                OutputLog.debug(f'{self.BAPDID} - {installresult}')
                                self.DeviceStatus['loaded'] = False
                                device_connection.disconnect()
                                return

                except OSError:
                    OutputLog.error(f'{self.BAPDID} did not copy the file within the expected timing!')
                    self.DeviceStatus['loaded'] = False
                    device_connection.disconnect()
                    return

                device_connection.disconnect()

            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                # NetmikoProfile = 'cisco_wlc_ssh'
                # showconfig = "show run-config"
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['loaded'] = False

            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                # Select the image and assign the info to a variable
                self.TargetFirmware = ApprovedFirmware[TargetInfo['vendor']][TargetInfo['modules']['1']['model']]

                # Make sure it isn't already on the device
                if self.TargetFirmware['filename'] in self.FlashContents:
                    # Get the MD5 hash of the file
                    OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to check the hash of the file')
                    device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                    filehash = device_connection.send_command(f"verify /md5 disk0:/{self.TargetFirmware['filename']}")

                    device_connection.disconnect()

                    # Check if the hash is right. If it is, we're done
                    if self.TargetFirmware['fileMD5'] in filehash:
                        OutputLog.info(f"{self.BAPDID} already has the image, and a correct file hash")
                        self.DeviceStatus['loaded'] = True
                        return
                    else:
                        OutputLog.error(f"{self.BAPDID} already has the image, and an incorrect file hash!")
                        self.DeviceStatus['loaded'] = False
                        return

                # Make sure it isn't already on the secondary
                if TargetInfo['failover']:
                    if self.TargetFirmware['filename'] in self.SecondaryFlashContents:
                        # Get the MD5 hash of the file
                        OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to check the hash of the secondary')
                        device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                        secondaryfilehash = device_connection.send_command(f"failover exec mate verify /md5 disk0:/{self.TargetFirmware['filename']}")

                        device_connection.disconnect()

                        # Check if the hash is right. If it is, we're done
                        if self.TargetFirmware['fileMD5'] in secondaryfilehash:
                            OutputLog.info(f"{self.BAPDID} secondary has the image, and a correct file hash")
                            self.DeviceStatus['loaded'] = True
                            return
                        else:
                            OutputLog.error(f"{self.BAPDID} secondary has the image, and an incorrect file hash!")
                            self.DeviceStatus['loaded'] = False
                            return

                # Make sure there's enough free space
                if int(self.TargetFirmware['filesize']) < int(self.DeviceStatus['freespace']):
                    OutputLog.info(f'{self.BAPDID} has enough space for the image')
                else:
                    OutputLog.error(f"{self.BAPDID} doesn't have enough space for the image- {self.TargetFirmware['filesize']} needed and {self.DeviceStatus['freespace']} available!")
                    self.DeviceStatus['loaded'] = False
                    return

                # Make sure there's enough free on the secondary
                if TargetInfo['failover']:
                    if int(self.TargetFirmware['filesize']) < int(self.DeviceStatus['secondaryfreespace']):
                        OutputLog.info(f'{self.BAPDID} secondary has enough space for the image')
                    else:
                        OutputLog.error(f"{self.BAPDID} secondary doesn't have enough space for the image- {self.TargetFirmware['filesize']} needed and {self.DeviceStatus['secondaryfreespace']} available!")
                        self.DeviceStatus['loaded'] = False
                        return

                # Load the image
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to load the image')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                try:
                    if TargetInfo['failover']: device_connection.send_command(f"failover exec mate copy /noconfirm ftp://{SFTPServerInfo['username']}:{SFTPServerInfo['password']}@{SFTPServerInfo['ipaddress']}{self.TargetFirmware['filepath']}{self.TargetFirmware['filename']} disk0:/", delay_factor=12)
                    device_connection.send_command(f"copy /noconfirm ftp://{SFTPServerInfo['username']}:{SFTPServerInfo['password']}@{SFTPServerInfo['ipaddress']}{self.TargetFirmware['filepath']}{self.TargetFirmware['filename']} disk0:/", delay_factor=12)
                    filehash = device_connection.send_command(f"verify /md5 disk0:/{self.TargetFirmware['filename']}")
                    if TargetInfo['failover']: secondaryfilehash = device_connection.send_command(f"failover exec mate verify /md5 disk0:/{self.TargetFirmware['filename']}")
                except OSError:
                    OutputLog.info(f'{self.BAPDID} did not copy the file within 20 minutes- waiting a little bit longer')
                    time.sleep(600)

                    try:  # The connection may have closed, so we'll run this in a try
                        filehash = device_connection.send_command(f"verify /md5 disk0:/{self.TargetFirmware['filename']}")
                        if TargetInfo['failover']: secondaryfilehash = device_connection.send_command(f"failover exec mate verify /md5 disk0:/{self.TargetFirmware['filename']}")
                    except AttributeError:
                        OutputLog.info(f'DID {self.BAPDID}- recreating SSH session using profile {self.NetmikoProfile} to check the image load')
                        device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                        filehash = device_connection.send_command(f"verify /md5 disk0:/{self.TargetFirmware['filename']}")
                        if TargetInfo['failover']: secondaryfilehash = device_connection.send_command(f"failover exec mate verify /md5 disk0:/{self.TargetFirmware['filename']}")

                device_connection.disconnect()

                # Check if the hash is right. If it is, we're done
                if self.TargetFirmware['fileMD5'] in filehash:
                    OutputLog.info(f"{self.BAPDID} loaded properly")
                else:
                    OutputLog.error(f"{self.BAPDID} failed to load the image!")
                    self.DeviceStatus['loaded'] = False
                    return

                if TargetInfo['failover']:
                    if self.TargetFirmware['fileMD5'] in secondaryfilehash:
                        OutputLog.info(f"{self.BAPDID} secondary loaded properly")
                    else:
                        OutputLog.error(f"{self.BAPDID} secondary failed to load the image!")
                        self.DeviceStatus['loaded'] = False
                        return

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Extreme':
            # NetmikoProfile = 'extreme_exos'
            # showconfig = "show configuration"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            # Select the image and assign the info to a variable
            self.TargetFirmware = ApprovedFirmware[TargetInfo['vendor']][re.match('^[A-Z]{1,2}', TargetInfo['summaryversion']).group()]
            self.DeviceStatus['additionalhops'] = False

            # Make sure that hops are not necessary
            if self.TargetFirmware['required']['number'] is not 0:
                for Number in range(1, self.TargetFirmware['required']['number'] + 1):
                    if parse_version(self.DeviceStatus['currentimage']) < parse_version(self.TargetFirmware['required'][Number]['version']):
                        self.TargetFirmware = self.TargetFirmware['required'][Number]
                        self.DeviceStatus['additionalhops'] = True
                        OutputLog.info(f'DID {self.BAPDID}- selected {self.TargetFirmware["version"]} as a required hop')
                        break

            # Check if the image is already running
            if parse_version(self.DeviceStatus['currentimage']) < parse_version(self.TargetFirmware['version']):
                pass
            else:
                OutputLog.error(f'DID {self.BAPDID}- already running approved version or above!')
                self.DeviceStatus['loaded'] = False
                return

            # Make sure the primary and secondary match
            if self.DeviceStatus['primaryflash'] not in self.DeviceStatus['secondaryflash']:
                # Synchronize the images
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to synchronize the images')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
                if self.DeviceStatus['bootvar'] == 'primary':
                    copytarget = 'secondary'
                elif self.DeviceStatus['bootvar'] == 'secondary':
                    copytarget = 'primary'
                device_connection.send_command(f"copy flash flash {copytarget}", delay_factor=10)
                device_connection.disconnect()

            else:
                OutputLog.info(f'DID {self.BAPDID}- no need to synchronize images')

            # We need to cache the file locally to upload- create a temporary file
            OutputLog.info(f'DID {self.BAPDID}- locally caching {self.TargetFirmware["filename"]}')
            FirmwareFile = BytesIO()
            SFTPConnection = SFTPSetup(SFTPServerInfo)
            SFTPConnection.chdir(self.TargetFirmware['filepath'][1:])
            SFTPConnection.getfo(self.TargetFirmware["filename"], FirmwareFile)
            FirmwareFile.seek(0)  # Run back to the beginning of the file
            SFTPConnection.close()

            try:
                # Upload the image to the device- we have to upload into the active slot as older devices cannot change the next boot without reloading (cannot schedule)
                SSHConnection = paramiko.SSHClient()
                SSHConnection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                SSHConnection.connect(TargetInfo['ipaddress'], port=TargetInfo['adminport'], username=TargetInfo['username'], password=TargetInfo['password'])
                SCPConnection = SCPClient(SSHConnection.get_transport())
                SCPConnection.putfo(FirmwareFile, f'/os/{self.DeviceStatus["bootvar"]}')
                SSHConnection.close()
            except SCPException:
                OutputLog.error(f'DID {self.BAPDID}- unable to load {self.TargetFirmware["filename"]}!')
                self.DeviceStatus['loaded'] = False
                return

        elif TargetInfo['vendor'] == 'Mikrotik':
            # If the file is already downloaded, skip
            if "Downloaded, please reboot router to upgrade it" in self.DeviceStatus['checkupdatestatus']:
                self.DeviceStatus['loaded'] = True
                return

            # Start the connection to the device and trigger the download
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to download the image')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
            device_connection.send_command(f"system package update download once")
            time.sleep(60)
            downloadresults = device_connection.send_command(f"system package update print")

            # Make sure it downloaded
            if "Downloaded, please reboot router to upgrade it" in downloadresults:
                pass
            else:
                OutputLog.error(f'{self.BAPDID}- failed to download update- reported as {downloadresults}!')
                self.DeviceStatus['loaded'] = False
                return

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            # NetmikoProfile = 'extreme_wing_ssh'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False

        elif TargetInfo['vendor'] == 'Nomadix':
            # Select the image and assign the info to a variable
            self.TargetFirmware = ApprovedFirmware[TargetInfo['vendor']][TargetInfo['modules']['1']['model']]
            self.DeviceStatus['additionalhops'] = False

            # Make sure that hops are not necessary
            if self.TargetFirmware['required']['number'] is not 0:
                for Number in range(1, self.TargetFirmware['required']['number'] + 1):
                    if parse_version(TargetInfo['summaryversion']) < parse_version(self.TargetFirmware['required'][Number]['version']):
                        self.TargetFirmware = self.TargetFirmware['required'][Number]
                        self.DeviceStatus['additionalhops'] = True
                        OutputLog.info(f'DID {self.BAPDID}- selected {self.TargetFirmware["version"]} as a required hop')
                        break

            # We need to cache the file locally to upload- create a temporary file
            FirmwareFile = NamedTemporaryFile()

            OutputLog.info(f'DID {self.BAPDID}- locally caching {self.TargetFirmware["filename"]}')
            SFTPConnection = SFTPSetup(SFTPServerInfo)
            SFTPConnection.chdir(self.TargetFirmware['filepath'][1:])
            SFTPConnection.get(self.TargetFirmware["filename"], FirmwareFile.name)
            SFTPConnection.close()

            # Start the connection
            OutputLog.info(f'DID {self.BAPDID}- creating SFTP session using pexpect to load the image {self.TargetFirmware["filename"]}')
            device_connection = PExpectSFTPSetup(TargetInfo)

            # If the connection started correctly, proceed
            if device_connection:
                # Put the file
                CommandOutput = device_connection.sendline('put ' + FirmwareFile.name + ' ' + self.TargetFirmware["filename"])
                time.sleep(1)
                CommandOutput = device_connection.expect('sftp>', timeout=300)
                CommandOutput = device_connection.isalive()
                CommandOutput = device_connection.close()

                OutputLog.info(f'DID {self.BAPDID}- finished loading {self.TargetFirmware["filename"]}')

            else:
                self.DeviceStatus['loaded'] = False
                return

            FirmwareFile.close()

            # Check to see if failover is enabled, and if so alert the user that they should confirm the other unit is to be upgraded
            # if TargetInfo['failover']:
            #    OutputLog.error(f'DID {self.BAPDID}- failover is enabled- please verify that the second unit is scheduled!')

        elif TargetInfo['vendor'] == 'Ruckus':
            if self.DeviceStatus['type'] == 'ZoneDirector':
                OutputLog.info(f'DID {self.BAPDID}- determining target version')

                # We need to loop through the APs present to determine the proper image- start with a blank variable
                self.TargetFirmware = {}

                for APModel in TargetInfo['apmodels']:
                    try:
                        APFirmware = ApprovedFirmware[TargetInfo['vendor']][self.DeviceStatus['type']]['APs'][APModel[0]]
                    except KeyError:
                        OutputLog.error(f'DID {self.BAPDID}- unknown AP model of {APModel[0]}- cannot determine target firmware!')
                        self.DeviceStatus['loaded'] = False
                        return

                    # Check if this is the first model- if so we're done
                    if self.TargetFirmware:
                        if parse_version(self.TargetFirmware['version']) > parse_version(APFirmware['version']):
                            OutputLog.info(f'DID {self.BAPDID}- REPLACE- version {self.TargetFirmware["version"]} with {APFirmware["version"]} from AP {APModel[0]}')
                            self.TargetFirmware = APFirmware
                        else:
                            OutputLog.info(f'DID {self.BAPDID}- IGNORE- processed {APFirmware["version"]} from AP {APModel[0]}, version {self.TargetFirmware["version"]} remains')
                    else:
                        self.TargetFirmware = APFirmware
                        OutputLog.info(f'DID {self.BAPDID}- START- version {APFirmware["version"]} from AP {APModel[0]}')

                # Parse out the hardware series from the ZD model
                self.DeviceStatus['ZDSeries'] = f"{re.search('ZD11|ZD12|ZD3|ZD5', str(TargetInfo['modules']['1']['model'])).group():<06}"

                # Make sure the ZD itself is supported on the target
                try:
                    self.TargetFirmware[self.DeviceStatus['ZDSeries']]
                except KeyError:
                    self.TargetFirmware = ApprovedFirmware[TargetInfo['vendor']][self.DeviceStatus['type']]['ZDs'][self.DeviceStatus['ZDSeries']]
                    OutputLog.info(f'DID {self.BAPDID}- REPLACE- version {self.TargetFirmware["version"]} due to ZD support- model {self.DeviceStatus["ZDSeries"]}')

                # Verify that the target image is actually higher than the current one
                if parse_version(self.TargetFirmware['version']) > parse_version(TargetInfo['summaryversion']):
                    pass
                else:
                    OutputLog.error(f'DID {self.BAPDID}- target version {self.TargetFirmware["version"]} is not newer than current version {TargetInfo["modules"]["1"]["version"]}- will not continue!')
                    self.DeviceStatus['loaded'] = False
                    return

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['loaded'] = False

        else:
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['loaded'] = False
            return False

        if 'loaded' not in self.DeviceStatus.keys():
            self.DeviceStatus['loaded'] = True

        OutputLog.info(f'DID {self.BAPDID}- loading image successful')

    def SetAndSchedule(self, TargetInfo):

        # Select the commands for configuration items
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            # NetmikoProfile = 'ruckus_fastiron'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to schedule the reload')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                # Set the boot if needed
                if self.TargetFirmware['method'] == 'request':
                    if '.bin' in self.DeviceStatus['currentimage']:
                        device_connection.send_config_set(f'boot system switch all flash:/{self.TargetFirmware["filename"]}')
                        device_connection.send_config_set(f'no boot system switch all flash:/{self.DeviceStatus["bootvar"]}')
                        device_connection.send_command(f'write memory')

                # Determine the time until reload and trigger the reload
                ReloadIn = TimeCalculate(ReloadTime, 'hours')
                device_connection.send_command(f'reload in {ReloadIn} reason Scheduled_Maintenance', expect_string=r'Proceed with reload?')
                device_connection.send_command('\n', expect_string=r'#')

                device_connection.disconnect()

                OutputLog.info(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')
                print(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')

            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                # NetmikoProfile = 'cisco_wlc_ssh'
                # showconfig = "show run-config"
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['scheduled'] = False

            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                # Set the boot order
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to set the boot order')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                device_connection.send_config_set(f'boot system disk0:/{self.TargetFirmware["filename"]}')
                device_connection.send_config_set(f'no boot system disk0:/{self.DeviceStatus["currentimage"]}')
                device_connection.send_config_set(f'boot system disk0:/{self.DeviceStatus["currentimage"]}')
                device_connection.send_command(f'write memory')

                # Determine the time until reload and trigger the reload
                ReloadIn = TimeCalculate(ReloadTime, 'hours')
                device_connection.send_command(f'reload noconfirm in {ReloadIn} save-config reason Scheduled_Maintenance')

                if TargetInfo['failover']:
                    SecondaryReloadIn = TimeCalculate(ReloadTime, 'hours', 10)
                    device_connection.send_command(f'failover exec mate reload noconfirm in {SecondaryReloadIn} save-config reason Scheduled_Maintenance')

                device_connection.disconnect()

                OutputLog.info(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')
                print(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Extreme':
            # NetmikoProfile = 'extreme_exos'
            # showconfig = "show configuration"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            # Determine the time until reload and trigger the reload
            ReloadIn = TimeCalculate(ReloadTime, 'hours')
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to schedule the reload')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

            device_connection.send_command(f'write memory')

            if re.search('[2-9]', str(TargetInfo['modules'].keys())) is not None:  # Stacks cannot use "reload" as this only reboots the master unit
                if parse_version(re.search('[0-9].*', self.DeviceStatus['currentimage']).group()) > parse_version('16.01'):  # This version added a command scheduler
                    device_connection.send_config_set(f'job "Scheduled_Maintenance" delay {ReloadIn} "boot system"')
                else:
                    OutputLog.error(f'DID {self.BAPDID}- this stack is on {self.DeviceStatus["currentimage"]} and does not support scheduling- please perform this manually!')
                    self.DeviceStatus['scheduled'] = False
                    return
            else:  # Classic reload command will work here
                device_connection.send_command(f'reload after {ReloadIn}', expect_string=r'Reload scheduled in')
                device_connection.send_command(f'y')

            device_connection.disconnect()

            OutputLog.info(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')
            print(f'DID {self.BAPDID}- scheduled for reload in {ReloadIn}')

        elif TargetInfo['vendor'] == 'Mikrotik':
            # Determine the reload time
            ReloadIn = TimeCalculate(ReloadTime, 'seconds')

            # Convert the device dict to a JSON
            TargetJSON = json.dumps(TargetInfo)

            # Spawn the background process
            OutputLog.info(f'DID {self.BAPDID}- scheduling the device for reload')
            MikrotikReboot = Popen(['python3', '__mikrotikreboot__.py', str(self.BAPDID), str(ReloadIn), TargetJSON, self.NetmikoProfile], cwd=f'{os.path.dirname(os.path.realpath(__file__))}/Subscripts')

            # Check to see if the process spawned correctly
            time.sleep(1)
            try:
                MikrotikReboot.pid
                self.DeviceStatus['scheduled'] = True
            except AttributeError:
                OutputLog.error(f'DID {self.BAPDID}- background process failed to start!')
                self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            # NetmikoProfile = 'extreme_wing_ssh'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Nomadix':
            # Determine the reload time
            ReloadIn = TimeCalculate(ReloadTime, 'seconds')

            # Convert the device dict to a JSON
            TargetJSON = json.dumps(TargetInfo)

            # Spawn the background process
            OutputLog.info(f'DID {self.BAPDID}- scheduling the device for reload')
            NomadixReboot = Popen(['python3', '__nomadixupgrade__.py', str(self.BAPDID), str(ReloadIn), TargetJSON, self.NetmikoProfile, self.TargetFirmware['filename']], cwd=f'{os.path.dirname(os.path.realpath(__file__))}/Subscripts')

            # Check to see if the process spawned correctly
            time.sleep(1)
            try:
                NomadixReboot.pid
                self.DeviceStatus['scheduled'] = True
            except AttributeError:
                OutputLog.error(f'DID {self.BAPDID}- background process failed to start!')
                self.DeviceStatus['scheduled'] = False

        elif TargetInfo['vendor'] == 'Ruckus':
            if self.DeviceStatus['type'] == 'ZoneDirector':
                OutputLog.info(f'DID {self.BAPDID}- scheduling reload')

                # Determine the reload time
                ReloadIn = TimeCalculate(ReloadTime, 'seconds')

                # Convert the device dict to a JSON
                TargetJSON = json.dumps(TargetInfo)

                # Spawn the background process
                OutputLog.info(f'DID {self.BAPDID}- scheduling the device for reload')
                ControllerReboot = Popen(['python3', '__zonedirectorreboot__.py', str(self.BAPDID), str(ReloadIn), TargetJSON, self.NetmikoProfile], cwd=f'{os.path.dirname(os.path.realpath(__file__))}/Subscripts')

                # Check to see if the process spawned correctly
                time.sleep(1)
                try:
                    ControllerReboot.pid
                except AttributeError:
                    OutputLog.error(f'DID {self.BAPDID}- background process failed to start!')
                    self.DeviceStatus['scheduled'] = False
                    return

                # We need to add some time for the device to reboot
                ReloadIn = ReloadIn + 600

                ReloadedTime, Error = ControllerScheduleHops(TargetInfo, self.TargetFirmware, TargetInfo['summaryversion'], self.DeviceStatus['ZDSeries'], ReloadIn, self.NetmikoProfile)

                if Error:
                    self.DeviceStatus['scheduled'] = False
                    return

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['scheduled'] = False

        else:
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['scheduled'] = False
            return False

        if 'scheduled' not in self.DeviceStatus.keys():
            self.DeviceStatus['scheduled'] = True

        OutputLog.info(f'DID {self.BAPDID}- scheduling successful')

    def CancelAndReset(self, TargetInfo):

        # Select the commands for configuration items
        if TargetInfo['vendor'] == 'Aruba':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Brocade/Ruckus':
            # NetmikoProfile = 'ruckus_fastiron'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Cisco':
            if re.search('WS-C[0-9]{4}|WS-.*SUP', str(TargetInfo)) is not None:
                # Get the info from the device
                self.GatherDeviceInfo(TargetInfo)

                # Create the connection
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to cancel the update')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                # Cancel the reload
                if self.DeviceStatus['reloadscheduled']:
                    device_connection.send_command(f'reload cancel')

                # Reset the boot to the initial image
                if re.search('WS-C3650|WS-C3850', str(TargetInfo)) is not None:
                    OutputLog.error(f'{self.BAPDID} this is a 3650/3850- cancelled the reload but setting the boot is not supported!')
                    self.DeviceStatus['cancelled'] = False
                    device_connection.disconnect()

                else:
                    # Set the boot to the currently booted image
                    device_connection.send_config_set(f'boot system switch all {str(self.DeviceStatus["currentimage"])}')
                    device_connection.send_command('write memory')

                    device_connection.disconnect()

            elif re.search('AIR-CT[0-9]{4}', str(TargetInfo)) is not None:
                # NetmikoProfile = 'cisco_wlc_ssh'
                # showconfig = "show run-config"
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['cancelled'] = False

            elif re.search('ASA[0-9]{4}', str(TargetInfo)) is not None:
                # Get the info from the device
                self.GatherDeviceInfo(TargetInfo)

                # Create the connection
                OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to cancel the update')
                device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

                # Cancel the reload
                if self.DeviceStatus['reloadscheduled']:
                    device_connection.send_command(f'reload cancel')

                # Check to see if the current image is set to boot order 2
                if str(self.DeviceStatus['currentimage']) not in str(self.DeviceStatus['currentbootvar']).split(";")[1]:
                    OutputLog.error(f'{self.BAPDID}- failed to cancel update- please check this device!')
                    self.DeviceStatus['cancelled'] = False
                    device_connection.disconnect()
                    return

                # Remove the next boot image so the currently booted image is next boot
                device_connection.send_config_set(f'no boot system {str(self.DeviceStatus["currentbootvar"]).split(";")[0]}')
                device_connection.send_command('write memory')

                device_connection.disconnect()

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Colubris':
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Extreme':
            # NetmikoProfile = 'extreme_exos'
            # showconfig = "show configuration"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Hewlett-Packard':
            # Get the info from the device
            self.GatherDeviceInfo(TargetInfo)

            # Create the connection
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to cancel the update')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)

            # Cancel the reload and job if supported
            device_connection.send_command(f'no reload')
            if parse_version(self.DeviceStatus['currentimage']) > parse_version('16.01'):  # This version added a command scheduler
                device_connection.send_config_set(f'job "Scheduled_Maintenance" disable')

            # Check if the current image is in the active slot
            if str(self.DeviceStatus['currentimage']) not in str(self.DeviceStatus['activeflash']):
                # Make sure the current image is in one of the flash slots
                if self.DeviceStatus['currentimage'] in self.DeviceStatus['primaryflash'] or self.DeviceStatus['currentimage'] in self.DeviceStatus['secondaryflash']:
                    # Copy the image to the currently active slot
                    device_connection.send_command(f"copy flash flash {self.DeviceStatus['bootvar']}", delay_factor=10)

                else:
                    OutputLog.error(f'{self.BAPDID}- failed to cancel update- please check this device!')
                    self.DeviceStatus['cancelled'] = False
                    device_connection.disconnect()
                    return
            else:
                OutputLog.info(f'DID {self.BAPDID}- no need to synchronize images')

            device_connection.disconnect()

        elif TargetInfo['vendor'] == 'Mikrotik':
            # We have to cancel background reboots and remove the pending package upgrade from the device
            OutputLog.info(f'DID {self.BAPDID}- cancelling background tasks')
            BackgroundProcessCancel(self.BAPDID)

            # Start the connection to the device and trigger the download
            OutputLog.info(f'DID {self.BAPDID}- creating SSH session using profile {self.NetmikoProfile} to cancel the upgrade')
            device_connection, postloginbanner = SSHSetup(TargetInfo, self.NetmikoProfile)
            device_connection.send_command(f"system package update cancel")
            downloadresults = device_connection.send_command(f"system package update print")

            # Make sure it downloaded
            if "Downloaded, please reboot router to upgrade it" in downloadresults:
                OutputLog.error(f'{self.BAPDID}- failed to cancel update- please check this device!')
                self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Motorola/Extreme':
            # NetmikoProfile = 'extreme_wing_ssh'
            # showconfig = "show running-config"
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False

        elif TargetInfo['vendor'] == 'Nomadix':
            OutputLog.info(f'DID {self.BAPDID}- cancelling background tasks')
            BackgroundProcessCancel(self.BAPDID)

        elif TargetInfo['vendor'] == 'Ruckus':
            if 'ZD' in str(TargetInfo['modules']['1']['model']):
                OutputLog.info(f'DID {self.BAPDID}- cancelling background tasks')
                BackgroundProcessCancel(self.BAPDID)

            else:
                OutputLog.error(f'{self.BAPDID} is not currently supported!')
                self.DeviceStatus['cancelled'] = False

        else:
            OutputLog.error(f'{self.BAPDID} is not currently supported!')
            self.DeviceStatus['cancelled'] = False
            return False

        if 'cancelled' not in self.DeviceStatus.keys():
            self.DeviceStatus['cancelled'] = True


if __name__ == "__main__":  # If the script is called manually, take the input as variables and run the function

    import pprint  # For readability
    from joblib import Parallel, delayed  # Joblib allows multithreading

    Targets = sys.argv[1]  # DIDs to handle
    RunOptions = sys.argv  # Catch all input to check for passed commands

    # Check if a job number was provided- if not, default to -1 (number of local threads)
    if 'jobs=' in str(RunOptions):
        Jobs = int(re.search('(?<=jobs=)[0-9-]+', str(RunOptions)).group())
    else:
        Jobs = -1

    # Run the main script
    UpdateResults = Parallel(n_jobs=Jobs, verbose=10)(delayed(DeviceUpdate().Handler)(TargetInfo) for TargetInfo in Targets.split(","))

    # Print back the requisite info
    pprint.pprint(UpdateResults)
