# Overnight Tools

These tools assume that the setup from ScriptManager has been run

## DeviceID

Identify devices based on device IDs or HSOs

### Currently supported devices:

##### SNMP Only

* Cisco WLC

* Extreme switch

* Motorola/Extreme WING WLC

* Ruckus SmartZone

* Watchguard firewall

##### SNMP and SSH

* Aruba WLC

* Brocade/Ruckus ICX switch

* Cisco ASA, ISR, IOS/IOS-XE switch

* Colubris/HP MSM WLC

* HP Procurve switch

* Mikrotik RouterOS

* Nomadix NSE

* Ruckus ZoneDirector

### Behaviour

* Accepts a comma-separated list of targets

* Gathers management information utilizing DIDGather

* Skips devices that are last marked as down or monitoring disabled

* SNMP is attempted by default, then proceeds to SSH if SNMP fails

* Parallel threads are handled by joblib

* Aggregates counts of AP models for controllers

* Output is a list of dicts, one per device

* Output is printed using prettyprint for readability

### Options

###### filter=*list,of,items*
>Items to display in the output (discarding the rest)- the default is not set, but bapdid is guaranteed to be present
>
>The items do not need to be present in every device- for instance filtering by summaryversion may not show a result on every device if a device did not reply
>
>These items must match the keys present in the output at the top level (no nested dicts)
>
>Example usage: `overnighttools/deviceid.py 123456 filter=hostname,ipaddress`

###### jobs=*int*
>The number of threads for Parallel to use- the default is -1 (matches the thread count of the local system)
>
>Large numbers of threads can delay the execution as the threads do spawn regardless of the number of tasks to complete
>
>Example usage: `overnighttools/deviceid.py 123456 jobs=10`

###### sort=*list,of,items*
>Items to sort by- the default is snmp_reply, ssh_reply
>
>The items are sorted in the order they are provided- for example the default will order by SNMP result first, then SSH
>
>These items must match the keys present in the output at the top level (no nested dicts). If the key is not present, the default sort will be used
>
>Example usage: `overnighttools/deviceid.py 123456 sort=vendor,hostname`

##### Additional options
Below options need to be added after *options=* and can be separated by commas

Example usage: `overnighttools/deviceid.py 123456 options=hso,raw`

###### cli
>Skip SNMP and attempt only CLI
>
>Will mark snmp_reply as 'Skipped'
>
>Example usage: `overnighttools/deviceid.py 1234 options=cli`

###### hso
>Instead of a single device, this indicates that an entire account is the target
>
>Provide an account ID instead of a device ID
>
>Example usage: `overnighttools/deviceid.py 1234 options=hso`

###### mon
>Ignore monitoring status
>
>This will usually result in more failed attempts as many will not reply, but useful for new devices
>
>Example usage: `overnighttools/deviceid.py 1234 options=mon`

###### raw
>Use 'print' instead of prettyprint
>
>Output is displayed one device per line- useful for writing to file
>
>Example usage: `overnighttools/deviceid.py 1234 options=raw`

###### stat
>Ignore last device state
>
>Useful for post-upgrade runs where monitoring is catching up
>
>Example usage: `overnighttools/deviceid.py 1234 options=stat`

### Sample Output

    overnighttools/deviceid.py 123456
    [Parallel(n_jobs=-1)]: Using backend LokyBackend with 4 concurrent workers.
    [Parallel(n_jobs=-1)]: Done   1 tasks      | elapsed:    4.5s
    [Parallel(n_jobs=-1)]: Done   1 out of   1 | elapsed:    4.5s finished
    [{'adminport': 23456,
      'adminproto': 3,
      'bapdid': 123456,
      'baphostname': 'CORE_SWITCH',
      'baplaststatus': True,
      'bapmonitortype': 101,
      'hostname': 'CORESWITCH.sample.com',
      'hsoid': 1234,
      'ipaddress': '66.210.227.67',
      'modules': {'1': {'hardwarerev': 'V05',
                        'model': 'WS-C2960X-24PS-L',
                        'serial': 'ABCDEFGHIJKL',
                        'version': '15.0(2)EX5'}},
      'password': 'password',
      'snmp_reply': 'Complete',
      'snmpcommunity': 'public',
      'snmpport': 33456,
      'ssh_reply': 'Skipped',
      'summaryversion': '15.0(2)EX5',
      'timezone': '-5',
      'username': 'admin',
      'vendor': 'Cisco'}]

## DeviceUpdate

Update devices based on device IDs

### Currently supported devices and what is gathered for backups

* Cisco ASA, IOS/IOS-XE switch - show running-config
> IOS-XE not supported for reset- will cancel reload but not return the boot order
* HP Procurve switch - show running-config

* Mikrotik RouterOS - export

* Nomadix NSE - current.txt, inatconf.txt, netconf.txt, nseconf.txt, RoomFileV2.txt, wanconf.txt

* Ruckus ZoneDirector - show config, also gathers the config.bak from the GUI

### Behaviour

* Accepts a comma-separated list of targets

* Gathers basic device information from DeviceID

* Skips devices that are last marked as down or monitoring disabled

* SSH is required to execute commands

* Parallel threads are handled by joblib

* Default output lists the last successful state of individual devices

* Optional full output is a list of dicts, one per device

* Output is printed using prettyprint for readability

### Order of Operations

1. Downloads a list of current approved firmware and paths from the SFTP server

2. Backs up the configurations and uploads them to SFTP. If no reload time is provided, this is where the script ends

3. Gathers information such as free storage space to determine later success

4. Prepare the device by clearing flash space

5. Load the image from the path given on the SFTP server. For devices where direct downloads from the device are not preferable or possible, such as Procurve switches, the script will create a local cache of the firmware to upload to the device

6. Set the boot order and reload time. For devices where reloads cannot be scheduled, a background process is spawned to trigger the reload at the applicable time

Optional- If called on to cancel the execution, will remove background processes, remove the scheduled reboot and set the boot order back to the currently booted image

### Options

###### *reload time*
>The time local to the user running the script to perform the reloads
>
>This must be the first argument provided
>
>If not provided, the configuration will be backed up and the execution ended
>
>Example usage: `overnighttools/deviceupdate.py 123456 02:00`

###### abort
>Cancels the execution of the provided devices
>
>Any background processes and scheduled reloads will be canceled and the boot order returned to the currently booted image
>
>Example usage: `overnighttools/deviceupdate.py 123456 abort`

###### config
>Backs up the configuration of the provided devices
>
>Identical to the default behaviour of running without a time provided
>
>Example usage: `overnighttools/deviceupdate.py 123456 config`

###### full_output
>Displays all generated output instead of the last successful state
>
>Example usage: `overnighttools/deviceupdate.py 123456 full_output`

###### local
>Backs up the configuration of the provided devices to a local folder instead of the SFTP server
>
>Creates the folder structure in the local shell's current working directory
>
>Example usage: `overnighttools/deviceupdate.py 123456 local`

###### jobs=*int*
>The number of threads for Parallel to use- the default is -1 (matches the thread count of the local system)
>
>Large numbers of threads can delay the execution as the threads do spawn regardless of the number of tasks to complete
>
>Example usage: `overnighttools/deviceupdate.py 123456 jobs=10`

### Sample Output

    overnighttools/deviceupdate.py 123456 08:00 full_output
    [Parallel(n_jobs=20)]: Using backend LokyBackend with 20 concurrent workers.
    DID 123456- scheduled for reload in 1:49
    [Parallel(n_jobs=20)]: Done   1 tasks      | elapsed: 27.9min
    [Parallel(n_jobs=20)]: Done   1 out of   1 | elapsed: 27.9min finished
    [{'bapdid': '123456',
      'bootvar': 'flash:/c2960x-universalk9-mz.150-2.EX5/c2960x-universalk9-mz.150-2.EX5.bin',
      'configbackup': True,
      'currentimage': 'flash:/c2960x-universalk9-mz.150-2.EX5/c2960x-universalk9-mz.150-2.EX5.bin',
      'deletedimages': ['flash:c2960x-universalk9-mz.150-2.EX4'],
      'infogathered': True,
      'loaded': True,
      'prepared': True,
      'scheduled': True}]